/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.file;

import java.io.IOException;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pm.data.ClassManager;
import pm.data.DataManager;
import pm.data.InterfaceManager;
import pm.data.Method;
import pm.data.Variable;
import pm.test_bed.Test_Save;


/**
 *
 * @author sajidkamal
 */
public class FileManagerIT {
    
    public FileManagerIT() {
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        DataManager data= new DataManager();
        FileManager manager= new FileManager();
        Test_Save test1 = new Test_Save();
        
        test1.UnitTestSetUp(data);
        manager.saveData(data, "./work/Test1.json");
        
       //********* TEST WITH INTERFACE*******************************8   
        DataManager data1 = new DataManager();
        test1.UnitTestSetUp(data1);
        ArrayList<ClassManager> classArray = data1.getClassList();
        ArrayList<InterfaceManager> interfaceArray = data1.getInterfaceList();
        
        InterfaceManager Draggable = new InterfaceManager();
        Draggable.setLocationX(450);
        Draggable.setLocationY(600);
        Draggable.setInterfaceName("Draggable");
        Draggable.setPackageName("packageTest");
        Method dragX= new Method("dragX","void",false,false,"public");
        Draggable.addMethod(dragX);
        interfaceArray.add(Draggable);
        
        ClassManager Rectangle = new ClassManager(330,480);
        Rectangle.setClassName("Rectangle");
        Rectangle.setPackageName("packageTest");
        Method dragXY= new Method("dragX","void",false,false,"public");
        Rectangle.addMethod(dragXY);
        classArray.add(Rectangle);
        Rectangle.addParent(Draggable);
        
     
        manager.saveData(data1, "./work/Test2.json");
        
        //***************CLASS WITH AN ABSTRACT CLASS*************************
        DataManager data2 = new DataManager();
        test1.UnitTestSetUp(data2);
        
        ArrayList<ClassManager> classArray1 = data2.getClassList();
        
        ClassManager project = new ClassManager(500,400);
        project.setClassName("Project");
        project.setPackageName("packageTest1");
        Method doHW= new Method("doHW","void",false,true,"public");
        doHW.addArg(new Variable("pencil","int",false,""));
        project.addMethod(doHW);
        Variable arrList = new Variable("hwList","ArrayList",false,"public");
        project.addVariable(arrList);
        classArray1.add(project);
        
        ClassManager CSE = new ClassManager(600,480);
        CSE.setClassName("ComputerScience");
        CSE.setPackageName("packageTest1");
        Method doHW1= new Method("doHW","void",false,false,"public");
        doHW1.addArg(new Variable("pencil","int",false,""));
        CSE.addMethod(doHW1);
        CSE.addParent(project);
        classArray1.add(CSE);
        
        
        manager.saveData(data2, "./work/Test3.json");
        
        
        
        
        
        
        
        
            
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    
    
    /**
     * Test of loadData method, of class FileManager.
     */
    @Test
    public void testLoadData() throws Exception {
        System.out.println("loadData");
        DataManager data = new DataManager();
        String filePath = "./work/Test1.json";
        FileManager instance = new FileManager();
        instance.loadData(data, filePath);
        ArrayList<ClassManager> classArray = data.getClassList();
        
        ClassManager threadExample = classArray.get(0);
        
        assertEquals("ThreadExample",threadExample.getClassName());
        System.out.println("Name of Class: "+threadExample.getClassName());
        
        assertEquals("Application",threadExample.getParentList().get(0).getName());
        System.out.println("Parent Name: "+threadExample.getParentList().get(0).getName());
        
       assertEquals("300.0", Double.toString(threadExample.classGrid.getLayoutX()));
        System.out.println("LocationX: "+threadExample.classGrid.getLayoutX());
        assertEquals("500.0", Double.toString(threadExample.classGrid.getLayoutY()));
       System.out.println("LocationY: "+threadExample.classGrid.getLayoutY());
        
        assertEquals("testPackage",classArray.get(0).getPackageName());
        System.out.println("Package of Class: "+threadExample.getPackageName());
        
        assertEquals(false, classArray.get(0).checkAbstract());
        System.out.println("Class Abstract?: "+threadExample.checkAbstract());
        
        
        
        Variable window = threadExample.getVariableList().get(2);
        
        assertEquals("window",window.getName());
        System.out.println("Name of Variable:"+window.getName());
        
        assertEquals("Stage",window.getType());
        System.out.println("Type of Variable:"+window.getType());
        
        Method meth = threadExample.getMethodList().get(0);
        
        assertEquals("start",meth.getMethodName());
        System.out.println("Method Name:"+meth.getMethodName());
        
        Variable arg = meth.getMethodArgs().get(0);
        
        assertEquals("primaryStage",arg.getName());
        System.out.println("Method Arg Name:"+ arg.getName());
        assertEquals("Stage",arg.getType());
        System.out.println("Method Arg Type:"+arg.getType());

    }

    
    @Test
    public void testLoadDataInterface() throws Exception {
        System.out.println("\n \n");
        System.out.println("*****loadData with Interface******");
        DataManager data1 = new DataManager();
        String filePath = "./work/Test2.json";
        FileManager instance = new FileManager();
        instance.loadData(data1, filePath);
        
        ArrayList<ClassManager> classArray = data1.getClassList();
        ArrayList<InterfaceManager> interfaceArray = data1.getInterfaceList();
        
        InterfaceManager event = interfaceArray.get(1);
        ClassManager Rect = classArray.get(5);
        
        assertEquals("Draggable",event.getInterfaceName());
        System.out.println("InterfaceName: "+event.getInterfaceName());
        
        assertEquals("450",Integer.toString(event.getLocationX()));
        System.out.println("X-Coordinate: "+ event.getLocationX());
        
        assertEquals("600",Integer.toString(event.getLocationY()));
        System.out.println("Y-Coordinate: "+event.getLocationY());
        
        assertEquals("dragX",event.getInterfaceMethodList().get(0).getMethodName());
        System.out.println("Method Name: "+event.getInterfaceMethodList().get(0).getMethodName());
        
        assertEquals("Rectangle",Rect.getClassName());
        System.out.println("Class Name: "+Rect.getClassName());
        
        assertEquals(false,Rect.checkAbstract());
        System.out.println("Abstract?: "+Rect.checkAbstract());
        
        assertEquals("Draggable",Rect.getParentList().get(0).getName());
        System.out.println("Parent Name: "+Rect.getParentList().get(0).getName());
   
        //400,700
        assertEquals("330",Integer.toString(Rect.getStartPointX()));
        System.out.println("StartLineX:"+Rect.getStartPointX());
        
        assertEquals("480",Integer.toString(Rect.getStartPointY()));
        System.out.println("StartLineY:"+Rect.getStartPointY());
        
        assertEquals("450",Integer.toString(Rect.getEndPointX()));
        System.out.println("EndLineX: "+Rect.getEndPointX());
        
        assertEquals("600",Integer.toString(Rect.getEndPointY()));
        System.out.println("EndLineY: "+Rect.getEndPointY());
      
        
    }
    
    @Test
    public void testLoadDataAbstractClass() throws Exception {
        System.out.println("\n \n");
        System.out.println("*****loadData with Abstract******");
        DataManager data2 = new DataManager();
        String filePath = "./work/Test3.json";
        FileManager instance = new FileManager();
        instance.loadData(data2, filePath);
        
        ArrayList<ClassManager> classArray = data2.getClassList();
        
        ClassManager testing10= classArray.get(5);
        ClassManager testing11= classArray.get(6);
        
        assertEquals("Project",testing10.getClassName());
        System.out.println("Class Name: "+testing10.getClassName());
        
        assertEquals(true,testing10.checkAbstract());
        System.out.println("Abstract?: "+testing10.checkAbstract());
        
        assertEquals("500.0",Double.toString(testing10.classGrid.getLayoutX()));
        System.out.println("Location X:"+testing10.classGrid.getLayoutX());
        
        assertEquals("400.0",Double.toString(testing10.classGrid.getLayoutY()));
        System.out.println("LocationY:"+testing10.classGrid.getLayoutY());
        
        assertEquals("doHW",testing10.getMethodList().get(0).getMethodName());
        System.out.println("Method Name: "+testing10.getMethodList().get(0).getMethodName());
        
        assertEquals("void",testing10.getMethodList().get(0).getReturnType());
        System.out.println("Method Return Type"+testing10.getMethodList().get(0).getReturnType());
        
        assertEquals("pencil",testing10.getMethodList().get(0).getMethodArgs().get(0).getName());
        System.out.println("Method Arg Name:"+testing10.getMethodList().get(0).getMethodArgs().get(0));
        
        assertEquals("int",testing10.getMethodList().get(0).getMethodArgs().get(0).getType());
        System.out.println("Method Arg Type"+testing10.getMethodList().get(0).getMethodArgs().get(0).getType());
        
        assertEquals("500",Integer.toString(testing11.getEndPointX()));
        System.out.println("Line EndX"+testing10.getEndPointX());
                
        assertEquals("400",Integer.toString(testing11.getEndPointY()));
        System.out.println("Line EndY"+testing10.getEndPointY());
        
        assertEquals("600",Integer.toString(testing11.getStartPointX()));
        System.out.println("Line StartX: "+testing10.getStartPointX());
        
        assertEquals("480",Integer.toString(testing11.getStartPointY()));
        System.out.println("Line StartY: "+testing10.getStartPointY());
        
        
        
    }
    
}
