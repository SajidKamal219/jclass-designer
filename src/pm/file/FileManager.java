package pm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.text.Text;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.data.ClassManager;
import pm.data.DataManager;
import pm.data.ExternalClass;
import pm.data.InterfaceManager;
import pm.data.Method;
import pm.data.TypeChecking;
import pm.data.Variable;
import properties_manager.PropertiesManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;


/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Sajid Kamal
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {

    String imp;
    private static final ArrayList<String> primitiveList= new ArrayList<String>(Arrays.asList("int","double","float","byte","short","char","boolean","String","void"));

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     *
     * @param data The data management component for this application.
     *
     * @param filePath Path (including file name/extension) to where to save the
     * data to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;
        ArrayList<ClassManager> classArray = dataManager.getClassList();
        ArrayList<InterfaceManager> interfaceArray = dataManager.getInterfaceList();
        ArrayList<ExternalClass> externalList = dataManager.getExternalList();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (ClassManager c : classArray) {
            ArrayList<Variable> varList = c.getVariableList();
            JsonObject JSO;
            JSO = Json.createObjectBuilder()
                    .add("Type", "Class")
                    .add("x_coordinate", (int) (c.classGrid.getLayoutX()))
                    .add("y_coordinate", (int) (c.classGrid.getLayoutY()))
                    .add("Height", c.classGrid.getHeight())
                    .add("Width", c.classGrid.getWidth())
                    .add("Name", c.getClassName())
                    .add("packageName", c.getPackageName())
                    .add("Abstract",c.checkAbstract())
                    .add("Variables", makeVariablesArray(c))
                    .add("Methods", makeMethodsArray(c))
                    .add("Parent", checkParent(c))
                    .build();

            arrayBuilder.add(JSO);

        }
        for (InterfaceManager interfaces : interfaceArray) {
            JsonObject JSO5 = Json.createObjectBuilder()
                    .add("Type", "Interface")
                    .add("x_coordinate", (int)interfaces.getGridPane().getLayoutX())
                    .add("y_coordinate", (int)interfaces.getGridPane().getLayoutY())
                    .add("Height", interfaces.getGridPane().getHeight())
                    .add("Width", interfaces.getGridPane().getWidth())
                    .add("Name", interfaces.getInterfaceName())
                    .add("packageName", interfaces.getPackageName())
                    .add("Variables", makeVariablesArray(interfaces))
                    .add("Methods", makeMethodsArrayInterfaces(interfaces))
                    .add("Parent", checkParent(interfaces))
                    .build();
            arrayBuilder.add(JSO5);
        }
        for(ExternalClass ex: externalList){
            JsonObject JSO6 = Json.createObjectBuilder()
                    .add("Type", "ExternalClass")
                    .add("x_coordinate", (int)ex.getGridPane().getLayoutX())
                    .add("y_coordinate", (int)ex.getGridPane().getLayoutY())
                    .add("Height",(int) ex.getGridPane().getHeight())
                    .add("Width", (int) ex.getGridPane().getWidth())
                    .add("Name", ex.getName())
                    .add("Package",ex.getPackageName())
                    .build();
            arrayBuilder.add(JSO6);
        }

        JsonArray jA = arrayBuilder.build();

        StringWriter sw = new StringWriter();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add("UML", jA)
                .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);

        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    /**
     * This method loads data from a JSON formatted file into the data
     * management component and then forces the updating of the workspace such
     * that the user may edit the data.
     *
     * @param data Data management component where we'll load the file into.
     *
     * @param filePath Path (including file name/extension) to where to load the
     * data from.
     *
     * @throws IOException Thrown should there be an error reading in data from
     * the file.
     */
    public JsonArray makeVariablesArray(TypeChecking c) {
        ArrayList<Variable> varList = c.getVariableList();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (Variable v : varList) {
            JsonObject JSO1 = Json.createObjectBuilder()
                    .add("varName", v.getName())
                    .add("varType", v.getType())
                    .add("varAccess", v.getAccess())
                    .add("varStatic", v.getStatic())
                    .add("varPackageBox Info", varPackageBox(v))
                    .add("Uses Info",varUses(v))
                    .build();
            arrayBuilder.add(JSO1);
        }
        JsonArray jA = arrayBuilder.build();
        return jA;

    }
    public JsonArray varUses(Variable v){
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        if(v.varLineReturn!=null){
            JsonObject JSO3 = Json.createObjectBuilder()
                    .add("Parent Line", v.varLineReturn.getParentClass().getName())
                    .build();
            arrayBuilder.add(JSO3);
        }
        JsonArray jA = arrayBuilder.build();
        return jA;
    }
    
    public JsonArray varPackageBox(Variable v){
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        if(v.packageBox!=null){
             JsonObject JSO3 = Json.createObjectBuilder()
                     .add("PackageBox X", (int)v.packageBox.stack.getLayoutX())
                     .add("PackageBox Y", (int)v.packageBox.stack.getLayoutY())
                     .add("PackageBox Name", v.packageBox.getName())
                     .add("PackageBox import", v.packageBox.getImportName())
                     .build();
                     arrayBuilder.add(JSO3);
        }
         JsonArray jA = arrayBuilder.build();
        return jA;
    }

    public JsonArray methodArgsArray(Method m) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (Variable var : m.getMethodArgs()) {
            JsonObject JSO3 = Json.createObjectBuilder()
                    .add("ArgName", var.getName())
                    .add("ArgType", var.getType())
                    .add("Arg Uses", varUses(var))
                    .add("Arg Package Box", varPackageBox(var))
                    .build();
            arrayBuilder.add(JSO3);
        }
        JsonArray jA = arrayBuilder.build();
        return jA;
    }

    public JsonArray makeMethodsArray(TypeChecking c) {
        ArrayList<Method> methodList = c.getMethodList();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (Method m : methodList) {
            JsonObject JSO2 = Json.createObjectBuilder()
                    .add("methodName", m.getMethodName())
                    .add("methodReturnType", m.getReturnType())
                    .add("methodAccess", m.getAccess())
                    .add("methodStatic", m.isIsStatic())
                    .add("methodAbstract", m.isIsAbstract())
                    .add("Methods Arguments", methodArgsArray(m))
                    .add("Method Uses",methodUses(m))
                    .add("Method API", methodAPI(m))
                    .build();
            arrayBuilder.add(JSO2);
        }
        JsonArray jA = arrayBuilder.build();
        return jA;

    }
    
     public JsonArray methodUses(Method m){
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        if(m.methodLineReturn!=null){
            JsonObject JSO3 = Json.createObjectBuilder()
                    .add("Parent Line", m.methodLineReturn.getParentClass().getName())
                    .build();
            arrayBuilder.add(JSO3);
        }
        JsonArray jA = arrayBuilder.build();
        return jA;
    }
     public JsonArray methodAPI(Method m){
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        if(m.packageBox!=null){
             JsonObject JSO3 = Json.createObjectBuilder()
                     .add("PackageBox X", (int)m.packageBox.stack.getLayoutX())
                     .add("PackageBox Y", (int)m.packageBox.stack.getLayoutY())
                     .add("PackageBox Name", m.packageBox.getName())
                     .add("PackageBox import", m.packageBox.getImportName())
                     .build();
                     arrayBuilder.add(JSO3);
        }
         JsonArray jA = arrayBuilder.build();
        return jA;
     }

    public JsonArray checkParent(TypeChecking c) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        for (TypeChecking t : c.getParentList()) {
            JsonObject JSO2 = Json.createObjectBuilder()
                    .add("Parent Type:", t.getType())
                    .add("Parent Name:", t.getName())
                    .build();
            arrayBuilder.add(JSO2);
        }
        JsonArray jA = arrayBuilder.build();
        return jA;
    }

    public JsonArray makeMethodsArrayInterfaces(InterfaceManager c) {
        ArrayList<Method> methodList = c.getInterfaceMethodList();
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (Method m : methodList) {
            JsonObject JSO2 = Json.createObjectBuilder()
                    .add("methodName", m.getMethodName())
                    .add("methodReturnType", m.getReturnType())
                    .add("methodAccess", m.getAccess())
                    .add("methodStatic", m.isIsStatic())
                    .add("methodAbstract", m.isIsAbstract())
                    .add("Method Arguments", methodArgsArray(m))
                    .add("Method Uses",methodUses(m))
                    .add("Method API", methodAPI(m))
                    .build();
            arrayBuilder.add(JSO2);
        }
        JsonArray jA = arrayBuilder.build();
        return jA;

    }

    public void loadData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;

        JsonObject obj = loadJSONFile(filePath);
        JsonArray dataArray = obj.getJsonArray("UML");
        ArrayList<ClassManager> classList = dataManager.getClassList();
        ArrayList<InterfaceManager> interfaceList = dataManager.getInterfaceList();
        ArrayList<ExternalClass> externalList = dataManager.getExternalList();
        for (int i = 0; i < dataArray.size(); i++) {
            JsonObject object = dataArray.getJsonObject(i);
            String type = object.getString("Type");
            
            
            
            if (type.equals("Class")) {
                double x = (double) object.getInt("x_coordinate");
                double y = (double) object.getInt("y_coordinate");
                String name = object.getString("Name");
                String packageName = object.getString("packageName");

                ClassManager newClass = new ClassManager(x, y); // CREATE THE NEW CLASS
                newClass.setClassName(name);
                newClass.setPackageName(packageName);

                JsonArray varArray = object.getJsonArray("Variables");
                for (int j = 0; j < varArray.size(); j++) {
                    JsonObject object1 = varArray.getJsonObject(j);
                    String varName = object1.getString("varName");
                    String varType = object1.getString("varType");
                    String varAccess = object1.getString("varAccess");
                    boolean varStatic = object1.getBoolean("varStatic");
                    Variable var = new Variable(varName, varType, varStatic, varAccess);
                    newClass.getVariableList().add(var);
                    Text varText = new Text(var.toString());
                    newClass.getVarBox().getChildren().add(varText);
                    JsonArray varUsesArray = object1.getJsonArray("Uses Info");
                   if(!primitiveList.contains(var.getType())& varUsesArray.isEmpty()){
                       dataManager.selectedClass=newClass;
                    dataManager.addPackageBox(var);
                        JsonArray varPackageArray = object1.getJsonArray("varPackageBox Info");
                        if(!varPackageArray.isEmpty()){
                          JsonObject object2 = varPackageArray.getJsonObject(0);
 
                        int packageX= object2.getInt("PackageBox X");
                        int packageY= object2.getInt("PackageBox Y");
                        String packName = object2.getString("PackageBox Name");
                        String importName = object2.getString("PackageBox import");
                        
                        var.packageBox.stack.setLayoutX(packageX);
                        var.packageBox.stack.setLayoutY(packageY);
                        var.packageBox.setName(packName);
                        var.packageBox.setImportName(importName);
                        }
                }

                }

                JsonArray methodArray = object.getJsonArray("Methods");
                for (int k = 0; k < methodArray.size(); k++) {
                    JsonObject object2 = methodArray.getJsonObject(k);
                    String methodName = object2.getString("methodName");
                    String methodReturn = object2.getString("methodReturnType");
                    String methodAccess = object2.getString("methodAccess");
                    boolean methodStatic = object2.getBoolean("methodStatic");
                    boolean methodAbstract = object2.getBoolean("methodAbstract");
                    Method meth = new Method(methodName, methodReturn, methodStatic, methodAbstract, methodAccess);
                    JsonArray methUses = object2.getJsonArray("Method Uses");
                      if(!primitiveList.contains(meth.getReturnType())& methUses.isEmpty()){
                       dataManager.selectedClass=newClass;
                    dataManager.setUses(meth);
                        JsonArray methPackageArray = object2.getJsonArray("Method API");
                        if(!methPackageArray.isEmpty()){
                          JsonObject object3 = methPackageArray.getJsonObject(0);
                        int packageX= object3.getInt("PackageBox X");
                        int packageY= object3.getInt("PackageBox Y");
                        String packName = object3.getString("PackageBox Name");
                        String importName = object3.getString("PackageBox import");
                        
                        meth.packageBox.stack.setLayoutX(packageX);
                        meth.packageBox.stack.setLayoutY(packageY);
                        meth.packageBox.setName(packName);
                        meth.packageBox.setImportName(importName);
                        }
                }
                    
                    
                    
                    
                    JsonArray methodArgArray = object2.getJsonArray("Methods Arguments");
                    
                    for (int z = 0; z < methodArgArray.size(); z++) {
                        JsonObject object3 = methodArgArray.getJsonObject(z);
                        String argName = object3.getString("ArgName");
                        String argType = object3.getString("ArgType");
                        Variable arg = new Variable(argName, argType, false, "");
                        meth.addArg(arg);
                        JsonArray argUses = object3.getJsonArray("Arg Uses");
                         if(!primitiveList.contains(arg.getType())& argUses.isEmpty()){
                       dataManager.selectedClass=newClass;
                    dataManager.addPackageBox(arg);
                        JsonArray argPackageArray = object3.getJsonArray("Arg Package Box");
                        if(!argPackageArray.isEmpty()){
                          JsonObject object4 = argPackageArray.getJsonObject(0);
                        int packageX= object4.getInt("PackageBox X");
                        int packageY= object4.getInt("PackageBox Y");
                        String packName = object4.getString("PackageBox Name");
                        String importName = object4.getString("PackageBox import");
                        
                        arg.packageBox.stack.setLayoutX(packageX);
                        arg.packageBox.stack.setLayoutY(packageY);
                        arg.packageBox.setName(packName);
                        arg.packageBox.setImportName(importName);
                        meth.methodText.setText(meth.toString());
                        }
                        
                         }
                        
                    }
                    
                    //String name, String type, boolean staticBool, boolean abstractBool, String access, String args1,String args2 ){
                    newClass.addMethod(meth);
                    
                }
             

                classList.add(newClass);
            }

            
            
            
            
            
            else if (type.equals("Interface")) {

                int x = object.getInt("x_coordinate");
                int y = object.getInt("y_coordinate");
                String name = object.getString("Name");
                String packageName = object.getString("packageName");

                InterfaceManager loadInt = new InterfaceManager();
                loadInt.getGridPane().setLayoutX(x);
                loadInt.getGridPane().setLayoutY(y);
                loadInt.setInterfaceName(name);
                loadInt.setPackageName(packageName);
                
                
                JsonArray varArray = object.getJsonArray("Variables");
                for (int j = 0; j < varArray.size(); j++) {
                    JsonObject object1 = varArray.getJsonObject(j);
                    String varName = object1.getString("varName");
                    String varType = object1.getString("varType");
                    String varAccess = object1.getString("varAccess");
                    boolean varStatic = object1.getBoolean("varStatic");
                    Variable var = new Variable(varName, varType, varStatic, varAccess);
                    loadInt.getVariableList().add(var);
                    Text varText = new Text(var.toString());
                    loadInt.getVarBox().getChildren().add(varText);
                    JsonArray methUses = object1.getJsonArray("Uses Info");
                   if(!primitiveList.contains(var.getType())&methUses.isEmpty()){
                       dataManager.selectedClass=loadInt;
                    dataManager.addPackageBox(var);
                        JsonArray varPackageArray = object1.getJsonArray("varPackageBox Info");
                        if(!varPackageArray.isEmpty()){
                          JsonObject object2 = varPackageArray.getJsonObject(0);
                        int packageX= object2.getInt("PackageBox X");
                        int packageY= object2.getInt("PackageBox Y");
                        String packName = object2.getString("PackageBox Name");
                        String importName = object2.getString("PackageBox import");
                        
                        var.packageBox.stack.setLayoutX(packageX);
                        var.packageBox.stack.setLayoutY(packageY);
                        var.packageBox.setName(packName);
                        var.packageBox.setImportName(importName);
                        }
                }

                }

                JsonArray methodArray = object.getJsonArray("Methods");
                for (int k = 0; k < methodArray.size(); k++) {
                    JsonObject object2 = methodArray.getJsonObject(k);
                    String methodName = object2.getString("methodName");
                    String methodReturn = object2.getString("methodReturnType");
                    String methodAccess = object2.getString("methodAccess");
                    boolean methodStatic = object2.getBoolean("methodStatic");
                    boolean methodAbstract = object2.getBoolean("methodAbstract");
                    Method meth = new Method(methodName, methodReturn, methodStatic, methodAbstract, methodAccess);
                    
                       JsonArray methUses = object2.getJsonArray("Method Uses");
                      if(!primitiveList.contains(meth.getReturnType())& methUses.isEmpty()){
                       dataManager.selectedClass=loadInt;
                    dataManager.setUses(meth);
                        JsonArray methPackageArray = object2.getJsonArray("Method API");
                        if(!methPackageArray.isEmpty()){
                          JsonObject object3 = methPackageArray.getJsonObject(0);
                        int packageX= object3.getInt("PackageBox X");
                        int packageY= object3.getInt("PackageBox Y");
                        String packName = object3.getString("PackageBox Name");
                        String importName = object3.getString("PackageBox import");
                        
                        meth.packageBox.stack.setLayoutX(packageX);
                        meth.packageBox.stack.setLayoutY(packageY);
                        meth.packageBox.setName(packName);
                        meth.packageBox.setImportName(importName);
                        }
                }
                    JsonArray methodArgArray = object2.getJsonArray("Method Arguments");
                    
                    for (int b = 0; b < methodArgArray.size(); b++) {
                        JsonObject object3 = methodArgArray.getJsonObject(b);
                        String argName = object3.getString("ArgName");
                        String argType = object3.getString("ArgType");
                        Variable arg = new Variable(argName, argType, false, "");
                        meth.addArg(arg);
                        
                          JsonArray argUses = object3.getJsonArray("Arg Uses");
                         if(!primitiveList.contains(arg.getType())& argUses.isEmpty()){
                       dataManager.selectedClass=loadInt;
                    dataManager.addPackageBox(arg);
                        JsonArray argPackageArray = object3.getJsonArray("Arg Package Box");
                        if(!argPackageArray.isEmpty()){
                          JsonObject object4 = argPackageArray.getJsonObject(0);
                        int packageX= object4.getInt("PackageBox X");
                        int packageY= object4.getInt("PackageBox Y");
                        String packName = object4.getString("PackageBox Name");
                        String importName = object4.getString("PackageBox import");
                        
                        arg.packageBox.stack.setLayoutX(packageX);
                        arg.packageBox.stack.setLayoutY(packageY);
                        arg.packageBox.setName(packName);
                        arg.packageBox.setImportName(importName);
                        meth.methodText.setText(meth.toString());
                        }
                        
                         }
                        
                    }
                    
                    loadInt.getInterfaceMethodList().add(meth);
                    Text methText = new Text(meth.toString());
                    loadInt.getMethodBox().getChildren().add(methText);
                }

                interfaceList.add(loadInt);
            }

        }
        
        dataManager.loadClasses(classList,interfaceList,externalList);
        
        JsonArray dataArray1 = obj.getJsonArray("UML");
        for (int f = 0; f < dataArray1.size(); f++) {
       JsonObject object = dataArray1.getJsonObject(f);
       String type1 = object.getString("Type");
       if(type1.equals("Class")){
       JsonArray ParentArray = object.getJsonArray("Parent");
            for (int k = 0; k < ParentArray.size(); k++) {
             JsonObject object2 = ParentArray.getJsonObject(k);
                String parent= object2.getString("Parent Name:");
                String parentType= object2.getString("Parent Type:");
                
                
       ClassManager it= classList.get(f);
       dataManager.selectedClass= it;
       if(!parentType.equals("EXTERNAL")){
       dataManager.findParent(parent);
       }
       else{
           dataManager.handleExternalClass(parent);

       }
       
                
       }
        
       }
       if(type1.equals("Interface")){
            JsonArray ParentArray = object.getJsonArray("Parent");
            for (int k = 0; k < ParentArray.size(); k++) {
             JsonObject object2 = ParentArray.getJsonObject(k);
                String parent= object2.getString("Parent Name:");
       InterfaceManager inter= interfaceList.get(f);
       dataManager.selectedClass= inter;
       dataManager.findParent(parent);
       }
        
        }
        }

        JsonArray dataArray2 = obj.getJsonArray("UML");
        for (int f = 0; f < dataArray2.size(); f++) {
       JsonObject object = dataArray2.getJsonObject(f);
       String type = object.getString("Type");

       if(type.equals("ExternalClass")){
           
           String name = object.getString("Name");
           ExternalClass temp =null;
           for(ExternalClass ex: externalList){
               if(ex.getName().equals(name)){
                   temp=ex;
               }
           }
           int x_coord= object.getInt("x_coordinate");
           int y_coord = object.getInt("y_coordinate");
           int height= object.getInt("Height");
           int width= object.getInt("Width");
           String packageName = object.getString("Package");
           temp.getGridPane().setLayoutX(x_coord);
           temp.getGridPane().setLayoutY(y_coord);
           temp.getGridPane().setMinHeight(height);
           temp.getGridPane().setMinWidth(width);
           temp.setPackageName("packageName");
           
       }
       
        }
        for(ClassManager c: classList){
            for(Variable v: c.getVariableList()){
                if(v.packageBox==null & !primitiveList.contains(v.getType())){
                    dataManager.selectedClass=c;
                dataManager.addPackageBox(v);
                }
            }
            for(Method m: c.getMethodList()){
                if(m.packageBox==null& !primitiveList.contains(m.getReturnType())){
                    dataManager.selectedClass=c;
                    dataManager.setUses(m);
                }
                for(Variable var: m.getMethodArgs()){
                    if(var.packageBox==null & !primitiveList.contains(var.getType())){
                    dataManager.selectedClass=c;
                    dataManager.addPackageBox(var);
                    }
                }
            }
        }
        for(InterfaceManager i: interfaceList){
            for(Variable v: i.getVariableList()){
                    if(v.packageBox==null & !primitiveList.contains(v.getType())){
                        dataManager.selectedClass=i;
                        dataManager.addPackageBox(v);
                    }
            }
            for(Method m: i.getMethodList()){
                if(m.packageBox==null& !primitiveList.contains(m.getReturnType())){
                    dataManager.selectedClass=i;
                    dataManager.setUses(m);
                }
                for(Variable var: m.getMethodArgs()){
                    if(var.packageBox==null & !primitiveList.contains(var.getType())){
                    dataManager.selectedClass=i;
                    dataManager.addPackageBox(var);
                    }
                }
            }
            
        
    }
    }
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    /**
     * This method exports the contents of the data manager to a Web page
     * including the html page, needed directories, and the CSS file.
     *
     * @param data The data management component.
     *
     * @param filePath Path (including file name/extension) to where to export
     * the page to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        DataManager dataManager = (DataManager) data;
        ArrayList<File> dirArray = new ArrayList<File>();
        for (ClassManager c : dataManager.getClassList()) {
            String x = "";
            if (c.getPackageName().contains(".")) {
                String[] stringArray = c.getPackageName().split("\\.");
                for (int i = 0; i < stringArray.length; i++) {
                    x += stringArray[i] + "/";
                    File temporary = new File(filePath + "/" + x);

                    if (!temporary.exists()) {
                        temporary.mkdir();
                    }

                }
            } else {
                File notNested = new File(filePath + "/" + c.getPackageName());
                if (!notNested.exists()) {
                    notNested.mkdir();
                }
                x = "/" + c.getPackageName() + "/";
            }
            PrintWriter out = new PrintWriter(filePath + "/" + x + c.getClassName() + ".java");

            out.println("package " + c.getPackageName() + ";");
            out.println("");
            ArrayList<String> importList = new ArrayList<String>();

            for (Variable v : c.getVariableList()) {
                if(v.packageBox!=null){
                    if(!importList.contains(v.packageBox.getImportName()));
                   importList.add(v.packageBox.getImportName());
                }
                else if(v.varLineReturn!=null){
                    String y= v.varLineReturn.getParentClass().getPackageName()+"."+v.varLineReturn.getParentClass().getName();
                    if(!importList.contains(y)){
                        importList.add(y);
                    }
                }
            }
            for(Method m: c.getMethodList()){
                 if(m.packageBox!=null){
                    if(!importList.contains(m.packageBox.getImportName()));
                   importList.add(m.packageBox.getImportName());
                }
                else if(m.methodLineReturn!=null){
                    String y= m.methodLineReturn.getParentClass().getPackageName()+"."+m.methodLineReturn.getParentClass().getName();
                    if(!importList.contains(y)){
                        importList.add(y);
                    }
                }
                 for(Variable v: m.getMethodArgs()){
                  if(v.packageBox!=null){
                    if(!importList.contains(v.packageBox.getImportName()));
                   importList.add(v.packageBox.getImportName());
                }
                else if(v.varLineReturn!=null){
                    String y= v.varLineReturn.getParentClass().getPackageName()+"."+v.varLineReturn.getParentClass().getName();
                    if(!importList.contains(y)){
                        importList.add(y);
                    }
                }
                 }
            }
            
            for(TypeChecking d: c.getParentList()){
                if(!d.getType().equals("EXTERNAL")){
                    if(!importList.contains(d.getPackageName()+"."+d.getName())){
                    importList.add(d.getPackageName()+"."+d.getName());
                    }
                }
                else{
                    importList.add(d.getPackageName());
                }
                
                
            }
            

            for (String s : importList) {
                out.println("import " + s + ";");
            }



            out.println(""); // JUST PRINT OUT EXTRA NEW LINE
            if (c.checkAbstract()) {
                out.print("public abstract class " + c.getClassName());
            } else {
                out.print("public class " + c.getClassName());
            }

            if (c.getParentList() != null) {
                for (TypeChecking s : c.getParentList()) {
                    if (s.getType().equals("Class")) {
                        out.print(" extends " + s.getName());
                    }
                }

                String listOfInterfaces = "";
                boolean hasInter = false;
                for (TypeChecking d : c.getParentList()) {
                    if (d.getType().equals("Interface")) {
                        hasInter = true;
                        listOfInterfaces += " " + d.getName() + ",";

                    }
                }
                if (hasInter) {
                    out.print(" implements" + listOfInterfaces.substring(0, listOfInterfaces.length() - 1));
                }

                
            }

            out.println("{ \n");
            for (Variable v : c.getVariableList()) {
                out.print(v.varCodeString("Class"));
                out.println("");
            }
            out.println("");

            for (Method m : c.getMethodList()) {
                if (m.isIsAbstract()) {
                    out.println(m.methodIntCodeString());
                } else {
                    out.println(m.methodCodeString());
                }
            }

            out.println("} \n");

            out.close();
        }
        for (InterfaceManager i : dataManager.getInterfaceList()) {
            String x = "";
            if (i.getPackageName().contains(".")) {
                String[] stringArray1 = i.getPackageName().split("\\.");
                for (int k = 0; k < stringArray1.length; k++) {
                    x += stringArray1[k] + "/";
                    File temporary = new File(filePath + "/" + x);

                    if (!temporary.exists()) {
                        temporary.mkdir();
                    }

                }
            } else {
                File notNested = new File(filePath + "/" + i.getPackageName());
                if (!notNested.exists()) {
                    notNested.mkdir();
                }
                x = "/" + i.getPackageName() + "/";
            }
            PrintWriter out = new PrintWriter(filePath + "/" + x + i.getInterfaceName() + ".java");

            out.println("package " + i.getPackageName() + ";");
            out.println(" ");

            ArrayList<String> importList1 = new ArrayList<String>();
            
             for (Variable v : i.getVariableList()) {
                if(v.packageBox!=null){
                    if(!importList1.contains(v.packageBox.getImportName()));
                   importList1.add(v.packageBox.getImportName());
                }
                else if(v.varLineReturn!=null){
                    String y= v.varLineReturn.getParentClass().getPackageName()+"."+v.varLineReturn.getParentClass().getName();
                    if(!importList1.contains(y)){
                        importList1.add(y);
                    }
                }
            }
            for(Method m: i.getMethodList()){
                 if(m.packageBox!=null){
                    if(!importList1.contains(m.packageBox.getImportName()));
                   importList1.add(m.packageBox.getImportName());
                }
                else if(m.methodLineReturn!=null){
                    String y= m.methodLineReturn.getParentClass().getPackageName()+"."+m.methodLineReturn.getParentClass().getName();
                    if(!importList1.contains(y)){
                        importList1.add(y);
                    }
                }
                 for(Variable v: m.getMethodArgs()){
                  if(v.packageBox!=null){
                    if(!importList1.contains(v.packageBox.getImportName()));
                   importList1.add(v.packageBox.getImportName());
                }
                else if(v.varLineReturn!=null){
                    String y= v.varLineReturn.getParentClass().getPackageName()+"."+v.varLineReturn.getParentClass().getName();
                    if(!importList1.contains(y)){
                        importList1.add(y);
                    }
                }
                 }
            }
            
            for(TypeChecking d: i.getParentList()){
                if(!d.getType().equals("EXTERNAL")){
                    if(!importList1.contains(d.getPackageName()+"."+d.getName())){
                    importList1.add(d.getPackageName()+"."+d.getName());
                    }
                }
                else{
                    importList1.add(d.getPackageName());
                }
                
                
            }
            

            for (String s : importList1) {
                out.println("import " + s + ";");
            }

            out.print("public interface " + i.getInterfaceName()+" ");
            
            String listOfInterfaces = "";
                boolean hasInter = false;
                for (TypeChecking d : i.getParentList()) {
                    if (d.getType().equals("Interface")) {
                        hasInter = true;
                        listOfInterfaces += " " + d.getName() + ",";

                    }
                }
                if (hasInter) {
                    out.print(" implements" + listOfInterfaces.substring(0, listOfInterfaces.length() - 1));
                }
                out.print("{ \n");
            
             for(Variable v: i.getVariableList()){
                 out.print(v.varCodeString("Interface"));
                 out.println("");
             }
             
            
            for (Method m : i.getInterfaceMethodList()) {
                out.println(m.methodIntCodeString());

            }

            out.println("} \n");

            out.close();

        }
    }
    
    

    /**
     * This method is provided to satisfy the compiler, but it is not used by
     * this application.
     */
    public void importData(AppDataComponent data, String filePath) throws IOException {
        // NOTE THAT THE Web Page Maker APPLICATION MAKES
        // NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
        // EXPORTED WEB PAGES
    }

    public String findImport(String className) {
        Package[] pack = Package.getPackages();
        String y = className;
        imp = null;

        for (int i = 0; i < pack.length; i++) {

            Package t = Package.getPackage(pack[i].getName());
            try {
                Class x = Class.forName(t.getName() + "." + y, false, ClassLoader.getSystemClassLoader());
                imp = x.getName();

            } catch (ClassNotFoundException s) {
                // GO BACK TO LOOP
            }

        }
        return imp;
    }

    public String findPath(String path) {
        String[] temp = path.split("\\.");
        String x = "";
        for (int i = 0; i < temp.length; i++) {
            x += temp[i] + "/";
        }
        return x;
    }
    

}
