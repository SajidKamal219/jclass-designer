/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.gui;

import java.util.ArrayList;
import java.util.Arrays;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pm.data.DataManager;
import pm.data.Method;
import pm.data.Variable;

/**
 *
 * @author Sajid Kamal
 */
public class methodArgDialog  {
    Stage argStage;
    Scene dialogScene;
    Button addButton;
    Button closeButton;
    Button removeButton;
    ObservableList<Variable> arguments;
    TableView<Variable> argTable;
    DataManager manager;
    private static final ArrayList<String> primitiveList= new ArrayList<String>(Arrays.asList("int","double","float","byte","short","char","boolean","String"));
    
    public methodArgDialog(DataManager data){
    argStage= new Stage();
    
    argStage.initModality(Modality.APPLICATION_MODAL);
   
    manager= data;
    
    VBox dialogBox = new VBox();
    Label messageText = new Label("Method Arguments");
    messageText.setAlignment(Pos.CENTER);
    argTable = new TableView();
argTable.setEditable(true);
     TableColumn<Variable,String> argName= new TableColumn("Name");
         argName.setSortable(false);
         argName.setEditable(true);
         argName.setCellValueFactory(
            new PropertyValueFactory<>("Name"));
          argName.setCellFactory(TextFieldTableCell.forTableColumn());
           argName.setOnEditCommit(
            (TableColumn.CellEditEvent<Variable, String> t) -> {
                ((Variable) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setVarName(t.getNewValue());
        });
          
          
         TableColumn<Variable,String> argType = new TableColumn<>("Type");
         argType.setSortable(false);
         argType.setEditable(true);
         argType.setCellValueFactory(
            new PropertyValueFactory<>("Type"));
          argType.setCellFactory(TextFieldTableCell.<Variable>forTableColumn());
          argType.setOnEditCommit(
            (TableColumn.CellEditEvent<Variable, String> t) -> {
                ((Variable) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setType(t.getNewValue());
                Variable v= t.getTableView().getSelectionModel().getSelectedItem();
                if(!primitiveList.contains(v.getType())){
                manager.addPackageBox(v);
                }
                
        });
          
          argTable.getColumns().addAll(argName,argType);
          
          HBox ButtonPane = new HBox();
          addButton= new Button("Add Argument");
          
          removeButton = new Button("Remove Argument");
       
          
          closeButton = new Button("Close");
          closeButton.setOnAction(e->{
              argStage.close();
          });
          
          ButtonPane.getChildren().addAll(addButton,removeButton,closeButton);
          
          dialogBox.getChildren().addAll(messageText,argTable,ButtonPane);
          
          dialogBox.setPadding(new Insets(40, 60, 40, 60));
          //dialogBox.setSpacing(20);
          dialogScene = new Scene(dialogBox);
          argStage.setScene(dialogScene);
    }
    
   
    public void show(Method m){
       loadTable(m);
       
       addButton.setOnAction(e->{
           addArgHandler(m);
          });
       
        removeButton.setOnAction(e->{
              removeArg(m);
          });
       
        argStage.showAndWait();
        
        
    }
    public void addArgHandler(Method m){
        m.getMethodArgs().add(new Variable("test","",false,""));
        loadTable(m);
    }
    public void loadTable(Method m){
        arguments= FXCollections.observableArrayList(m.getMethodArgs());
       argTable.setItems(arguments);
    }
    public void removeArg(Method m){
        Variable selectedVar= argTable.getSelectionModel().getSelectedItem();
        m.getMethodArgs().remove(selectedVar);
        manager.handleRemoveVar(selectedVar);
        loadTable(m);
        
    }
}
