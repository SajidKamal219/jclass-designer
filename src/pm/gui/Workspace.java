package pm.gui;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import static javafx.scene.input.KeyCode.T;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import static pm.PropertyType.CLASS_TOOLTIP;
import static pm.PropertyType.EXPORT_CODE_TOOLTIP;
import static pm.PropertyType.INTERFACE_TOOLTIP;
import static pm.PropertyType.REDO_TOOLTIP;
import static pm.PropertyType.REMOVE_TOOLTIP;
import static pm.PropertyType.RESIZE_TOOLTIP;
import static pm.PropertyType.SAVE_AS_TOOLTIP;
import static pm.PropertyType.SELECTION_TOOL_TOOLTIP;
import static pm.PropertyType.SNAPSHOT_TOOLTIP;
import static pm.PropertyType.UNDO_TOOLTIP;
import static pm.PropertyType.ZOOM_IN_TOOLTIP;
import static pm.PropertyType.ZOOM_OUT_TOOLTIP;
import pm.data.DataManager;
import pm.data.Method;
import pm.data.Variable;
import pm.file.FileManager;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppPropertyType.SAVE_TOOLTIP;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import saf.ui.AppMessageDialogSingleton;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    public Pane leftPane;
    public ScrollPane scrollPane;
   
    VBox rightPane;
    HBox classNameRow;
    HBox packageRow;
    HBox parentRow;
    VBox varRow;
    HBox varLabelRow;
    VBox varTableRow;
    VBox methodRow;
    HBox methodLabelRow;
    VBox methodTableRow;
    TextField classText;
    TextField packageText;
    Button Plus;
    Button Minus;
    Button methodPlus;
    Button methodMinus;
    TableView<Variable> varTable= new TableView<Variable>();
    Button saveAsButton;
    Button screenshotButton;
    Button exportCodeButton;
    VBox ExportBox;
    Button selectionToolButton;
    Button resizeButton;
    Button addClass;
    Button addInterface;
    Button removeButton;
    TableView <Method>methodTable= new TableView<Method>();
    Button undoButton;
    Button redoButton;
    Button zoomInButton;
    Button zoomOutButton;
    ArrayList<TextField> textFieldArray;
    final ContextMenu spaceError;
    final ContextMenu emptyError;
    final ContextMenu packageEmptyError;
    boolean grid;
    private final ObservableList<String> accessLevels = FXCollections.observableArrayList("public","private","protected");
    double maxZoom=1.4;
    double minZoom=0.7;
    
    
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
        DataManager manager= (DataManager) app.getDataComponent();
        workspace=new HBox();
        scrollPane= new ScrollPane();
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        leftPane=new Pane();
        
        scrollPane.setContent(leftPane);
        scrollPane.setVvalue(0.5);
        scrollPane.setHvalue(0.5);
        textFieldArray=new ArrayList();

        rightPane= new VBox();
        
        //RightPane Layout
        classNameRow=new HBox();
            Label classLabel = new Label("Class Name                ");
            classLabel.getStyleClass().add("subheading_label");
            classText= new TextField();
            classText.setPromptText("Enter Class Name");
        classNameRow.getChildren().addAll(classLabel,classText);
        
        packageRow= new HBox();
            Label packageLabel= new Label("Package                     ");
            packageLabel.getStyleClass().add("subheading_label");
            packageText= new TextField();
            packageText.setPromptText("Enter Package Name");
            packageRow.getChildren().addAll(packageLabel,packageText);
        parentRow= new HBox();
            Label parentLabel= new Label("Parent                        ");
            parentLabel.getStyleClass().add("subheading_label");
            ComboBox parentCombo = new ComboBox();
            parentRow.getChildren().addAll(parentLabel,parentCombo);
        
            
         varRow= new VBox();
         varLabelRow=new HBox();
         Label var= new Label("Variables");
         var.getStyleClass().add("subheading_label");
         Plus = new Button();
         Minus = new Button();
         setImages(Plus,"Plus.png");
         setImages(Minus,"Minus.png");
         varLabelRow.getChildren().addAll(var, Plus, Minus);
         
         varTableRow= new VBox();
           varTable.setEditable(true);
           
           
         Plus.setOnAction(e->{
             manager.handleAddVar();
         });
         Minus.setOnAction(e->{
             manager.handleRemoveVar();
         });
         
          
         TableColumn<Variable,String> varName= new TableColumn("Name");
         varName.setSortable(false);
         varName.setCellValueFactory(
            new PropertyValueFactory<>("Name"));
          varName.setCellFactory(TextFieldTableCell.forTableColumn());
           varName.setOnEditCommit(
            (CellEditEvent<Variable, String> t) -> {
                ((Variable) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setVarName(t.getNewValue());
        });
          
          
         TableColumn<Variable,String> varType = new TableColumn<>("Type");
         varType.setSortable(false);
         varType.setCellValueFactory(
            new PropertyValueFactory<>("Type"));
          varType.setCellFactory(TextFieldTableCell.<Variable>forTableColumn());
          varType.setOnEditCommit(
            (CellEditEvent<Variable, String> t) -> {
                ((Variable) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setType(t.getNewValue());
        });
     
         TableColumn<Variable,Boolean> varStatic = new TableColumn<>("Static");
         varStatic.setSortable(false);
         varStatic.setCellValueFactory(
            new PropertyValueFactory<>("Static"));

          varStatic.setCellFactory(col -> {
            CheckBoxTableCell<Variable, Boolean> cell = new CheckBoxTableCell<>(index -> {
                BooleanProperty active = new SimpleBooleanProperty(varTable.getItems().get(index).getStatic());
                active.addListener((obs, wasStatic, isNowStatic) -> {
                    Variable item = varTable.getItems().get(index);
                    item.setStatic(isNowStatic);
                });
                return active ;
            });
            return cell ;
        });

          varStatic.setEditable(true);
         
         
         TableColumn<Variable,String> varAccess = new TableColumn("Access");
         varAccess.setSortable(false);
         varAccess.setCellValueFactory(
            new PropertyValueFactory<>("Access"));
          varAccess.setCellFactory(ComboBoxTableCell.forTableColumn(accessLevels));
          varAccess.setOnEditCommit(
            (CellEditEvent<Variable, String> t) -> {
                ((Variable) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setAccess(t.getNewValue());
                       
        });
         varTable.getColumns().addAll(varName,varType,varStatic,varAccess);
         varTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
  
         
         varTableRow.getChildren().add(varTable);
         
         
         
         varRow.getChildren().addAll(varLabelRow,varTableRow);
         methodTable.setEditable(true);
         methodRow= new VBox();
         methodLabelRow= new HBox();
         Label method = new Label("Methods");
         method.getStyleClass().add("subheading_label");
         methodPlus= new Button();
         methodPlus.setOnAction(e->{
             manager.addMethod();
         });
         methodMinus= new Button();
         methodMinus.setOnAction(e->{
             manager.handleRemoveMethod();
             });
              setImages(methodPlus,"Plus.png");
         setImages(methodMinus,"Minus.png");
         
         
         methodLabelRow.getChildren().addAll(method,methodPlus,methodMinus);
         methodTableRow= new VBox();
         
    
         TableColumn<Method,String> methodName= new TableColumn("Name");
         methodName.setSortable(false);
         methodName.setCellValueFactory(
            new PropertyValueFactory<>("MethodName"));
          methodName.setCellFactory(TextFieldTableCell.forTableColumn());
           methodName.setOnEditCommit(
            (CellEditEvent<Method, String> t) -> {
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setMethodName(t.getNewValue());
        });
         TableColumn<Method,String> methodReturn= new TableColumn("Return");
         methodReturn.setSortable(false);
         methodReturn.setCellValueFactory(
            new PropertyValueFactory<>("ReturnType"));
          methodReturn.setCellFactory(TextFieldTableCell.forTableColumn());
           methodReturn.setOnEditCommit(
            (CellEditEvent<Method, String> t) -> {
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setReturnType(t.getNewValue());
        });
         TableColumn<Method,Boolean> methodStatic= new TableColumn("Static");
         methodStatic.setSortable(false);
         methodStatic.setCellValueFactory(
            new PropertyValueFactory<>("Static"));
          methodStatic.setCellFactory(col -> {
            CheckBoxTableCell<Method, Boolean> cell = new CheckBoxTableCell<>(index -> {
                BooleanProperty active = new SimpleBooleanProperty(methodTable.getItems().get(index).getStatic());
                active.addListener((obs, wasStatic, isNowStatic) -> {
                    Method item = methodTable.getItems().get(index);
                    item.setStatic(isNowStatic);
                });
                return active ;
            });
            return cell ;
        });
         
         TableColumn<Method,Boolean> methodAbstract = new TableColumn("Abstract");
         methodAbstract.setSortable(false);
         methodAbstract.setCellValueFactory(
            new PropertyValueFactory<>("Abstract"));
          methodAbstract.setCellFactory(col -> {
            CheckBoxTableCell<Method, Boolean> cell = new CheckBoxTableCell<>(index -> {
                BooleanProperty active = new SimpleBooleanProperty(methodTable.getItems().get(index).getAbstract());
                active.addListener((obs, wasAbstract, isNowAbstract) -> {
                    Method item = methodTable.getItems().get(index);
                    item.setAbstract(isNowAbstract);
                    manager.loadMethodTable(manager.selectedClass);

                });
                return active ;
            });
            return cell ;
        });
         
         TableColumn<Method,String> methodAccess = new TableColumn("Access");
         methodAccess.setSortable(false);
         methodAccess.setCellValueFactory(
            new PropertyValueFactory<>("Access"));
          methodAccess.setCellFactory(ComboBoxTableCell.forTableColumn(accessLevels));
          methodAccess.setOnEditCommit(
            (CellEditEvent<Method, String> t) -> {
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setAccess(t.getNewValue());
        });
         
        TableColumn<Method,String> methodArg1= new TableColumn("Args");
        methodArg1.setEditable(false);
        methodArg1.setCellValueFactory(
            new PropertyValueFactory<>("MethodArgString"));
         methodArg1.setCellFactory(TextFieldTableCell.forTableColumn());
         methodArg1.setOnEditCommit(
            (CellEditEvent<Method, String> t) -> {
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setDummy(t.getNewValue());
        });
         
    methodTable.setRowFactory( tv -> {
    TableRow<Method> row = new TableRow<>();
    row.setOnMouseClicked(event -> {
        if (event.getClickCount() == 3 && (! row.isEmpty()) ) {
            Method rowData = row.getItem();
            methodArgDialog dialog = new methodArgDialog();
            dialog.show(rowData);
            rowData.methodText.setText(rowData.toString());
        }
    });
    return row ;
});
         
       /*TableColumn<Method,String> methodArg1= new TableColumn("Arg1");
        methodArg1.setEditable(true);
        methodArg1.setCellValueFactory(
            new PropertyValueFactory<>("Arg1"));
         methodArg1.setCellFactory(TextFieldTableCell.forTableColumn());
          methodArg1.setOnEditCommit(
            (CellEditEvent<Method, String> t) -> {
                ((Method) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setArg1(t.getNewValue());
        });
*/
         
         TableColumn methodArg2= new TableColumn("Arg2");
         methodTable.getColumns().addAll(methodName, methodReturn, methodStatic,methodAbstract, methodAccess,methodArg1);//, methodArg1,methodArg2);
         methodTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

         methodTableRow.getChildren().add(methodTable);
         methodRow.getChildren().addAll(methodLabelRow,methodTableRow);
         
         
 
        rightPane.getChildren().addAll(classNameRow,packageRow,parentRow,varRow,methodRow);
        
       spaceError = new ContextMenu();
        spaceError.setAutoHide(false);
        spaceError.getItems().add(new MenuItem("Class Name cannot have spaces"));
        spaceError.hide();
        
        emptyError = new ContextMenu();
        emptyError.setAutoHide(false);
        emptyError.getItems().add(new MenuItem("Class Name cannot be empty"));
        emptyError.hide();
        
        packageEmptyError= new ContextMenu();
        packageEmptyError.setAutoHide(false);
        packageEmptyError.getItems().add(new MenuItem("Package name cannot be empty"));
        packageEmptyError.hide();
        
      
        
        workspace.getChildren().add(scrollPane);
        workspace.getChildren().add(rightPane);
       
        
       setFileToolBar();
       textFieldArray.add(classText);
       textFieldArray.add(packageText);
      
   
        
        
        
        
        
        
        
        
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
        Scene test=gui.getPrimaryScene();
        leftPane.setMinWidth(5000);
        leftPane.setMinHeight(5000);
        leftPane.prefWidthProperty().bind(test.widthProperty());
        leftPane.prefHeightProperty().bind(test.heightProperty());
        rightPane.setPrefWidth(1000);
	rightPane.getStyleClass().add("bordered_pane1");
        classNameRow.getStyleClass().add("bordered_pane");
        packageRow.getStyleClass().add("bordered_pane");
        parentRow.getStyleClass().add("bordered_pane");
        Plus.getStyleClass().add("trans");
        Minus.getStyleClass().add("trans1");
        methodPlus.getStyleClass().add("trans");
        methodMinus.getStyleClass().add("trans1");
        
        //SET THE LABEL STYLES
       
        
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {

    }
    
    
    public void setImages(Button button, String fileName) {
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + fileName;
        Image buttonImage = new Image(imagePath);
	
	// SET THE IMAGE IN THE BUTTON
        button.setGraphic(new ImageView(buttonImage));	
    }
   public void setFileToolBar(){
   DataManager manager= (DataManager) app.getDataComponent();
   FlowPane fileBar= app.getGUI().getFileToolBar();
   
   HBox three= new HBox();
   saveAsButton=initChildButton(three,"SaveAs.png",SAVE_AS_TOOLTIP.toString(),true);
   saveAsButton.setPrefHeight(35);
   screenshotButton= initChildButton(three,"Snapshot.png",SNAPSHOT_TOOLTIP.toString(),false);
   screenshotButton.setPrefHeight(35);
   screenshotButton.setOnAction(e->{
       ScreenshotHandler();
   });
   exportCodeButton= initChildButton(three,"Export.png",EXPORT_CODE_TOOLTIP.toString(),false);
   exportCodeButton.setOnAction(e-> {
       try {
           manager.handleExportCode();
       } catch (IOException ex) {
           Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
       }
       
   });
   exportCodeButton.setPrefHeight(35);
   
   
   HBox selectionPane = new HBox();
   selectionToolButton= initChildButton(selectionPane,"SelectionTool.png",SELECTION_TOOL_TOOLTIP.toString(),false);
   selectionToolButton.setOnAction(e->{
       updateButtons(true);
       manager.selectionToolHandler();
   });
   resizeButton= initChildButton(selectionPane,"resize.png",RESIZE_TOOLTIP.toString(),true);

  addClass=initChildButton(selectionPane,"classImage.png",CLASS_TOOLTIP.toString(),false);
  addClass.setPrefHeight(35);
  
  
  addClass.setOnAction(e ->{
      

      updateButtons(false);
      
      
      leftPane.setOnMouseClicked(e1->{
      
      manager.handleAddClass(leftPane,rightPane,e1.getX(),e1.getY());
      selectionToolButton.fire();
      });
           
      
  });
 
  addInterface= initChildButton(selectionPane,"interfaceImage.png",INTERFACE_TOOLTIP.toString(),false);
  addInterface.setPrefHeight(35);
  addInterface.setOnAction(e->{
      manager.handleAddInterface(leftPane, rightPane);
      updateButtons(false);
  });
  removeButton = initChildButton(selectionPane,"removeImage.png",REMOVE_TOOLTIP.toString(),false);
  removeButton.setPrefHeight(35);
  removeButton.setOnAction(e->{
     manager.handleRemoveButton(leftPane);
     selectionToolButton.setDisable(false);
  });
  undoButton= initChildButton(selectionPane,"Undo.png",UNDO_TOOLTIP.toString(),false);
  undoButton.setOnAction(e->{
       leftPane.setScaleX(1);
       leftPane.setScaleY(1);
  });
  redoButton= initChildButton(selectionPane,"Redo.png",REDO_TOOLTIP.toString(),false);
  undoButton.setPrefHeight(35);
  redoButton.setPrefHeight(35);
  
  HBox zoomPane= new HBox();
  zoomInButton= initChildButton(zoomPane,"ZoomIn.png",ZOOM_IN_TOOLTIP.toString(),false);
  zoomInButton.setOnAction(e->{
      
      if(leftPane.getScaleX()<maxZoom){
     leftPane.setScaleX(leftPane.getScaleX()+0.1);
     leftPane.setScaleY(leftPane.getScaleY()+0.1);
      }
     
   
     
  });
  zoomOutButton= initChildButton(zoomPane,"ZoomOut.png",ZOOM_OUT_TOOLTIP.toString(),false);
  zoomOutButton.setOnAction(e->{
      if(leftPane.getScaleX()>minZoom){
  leftPane.setScaleX(leftPane.getScaleX()-0.1);
  leftPane.setScaleY(leftPane.getScaleX()-0.1);
      }

  });
Button zoomNormalButton = initChildButton(zoomPane,"",ZOOM_OUT_TOOLTIP.toString(),false);
zoomNormalButton.setPrefHeight(35);
zoomNormalButton.setPrefWidth(55);
zoomNormalButton.setText("100%");
zoomNormalButton.setOnAction(e->{
leftPane.setScaleX(1);
leftPane.setScaleY(1);
});

  
  VBox gridBox= new VBox();
  HBox grid = new HBox();
  CheckBox gridCheck = new CheckBox();
gridCheck.selectedProperty().addListener(new ChangeListener<Boolean>() {
        public void changed(ObservableValue<? extends Boolean> ov,
            Boolean old_val, Boolean new_val) {
               if(gridCheck.isSelected()){
                   leftPane.getStyleClass().add("grid_lines");
               }
               else{
                   leftPane.getStyleClass().remove("grid_lines");
                   
                   
               }
               
        }
    });
  Text gridText = new Text("Grid");
  grid.getChildren().addAll(gridCheck,gridText);
  
  HBox snap = new HBox();
  CheckBox snapCheck = new CheckBox();
  Text snapText = new Text("Snap");
  snap.getChildren().addAll(snapCheck,snapText);
  
  gridBox.getChildren().addAll(grid,snap);
  
  zoomPane.getChildren().add(gridBox);
 
  
   
   fileBar.getChildren().addAll(three,selectionPane,zoomPane);
   
  
   three.getStyleClass().add("toolbar_pane");
   selectionPane.getStyleClass().add("toolbar_pane");
   zoomPane.getStyleClass().add("toolbar_pane");
   
   
 
 
    }
    
    public Button initChildButton(Pane toolbar, String icon, String tooltip, boolean disabled) {
    // LOAD THE ICON FROM THE PROVIDED FILE
    PropertiesManager props = PropertiesManager.getPropertiesManager();
    
    
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + icon;
        Image buttonImage = new Image(imagePath);

	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);

	// PUT THE BUTTON IN THE TOOLBAR
        toolbar.getChildren().add(button);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    public void ScreenshotHandler(){

      try{
       int height=(int)leftPane.getHeight();
       int width=(int)leftPane.getWidth();
       FileChooser fc=new FileChooser();
       fc.setInitialFileName("Class_Diagram");
       fc.getExtensionFilters().addAll(
		new FileChooser.ExtensionFilter("png","*.png"));
       WritableImage image=new WritableImage(width,height);
       WritableImage realImage=leftPane.snapshot(new SnapshotParameters(), image);
       File outputImage=fc.showSaveDialog(gui.getWindow()); 
       if (outputImage != null) {
		    ImageIO.write(SwingFXUtils.fromFXImage(realImage, null), "png", outputImage);
		}
       
      }
      catch(IOException s){
          AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
		dialog.show("Error", "There was an error saving your file");
      }
    }
    
    public void updateButtons(boolean update){
       selectionToolButton.setDisable(update);
    }
    public TextField getClassTextField(){
        return classText;
    }
    public TextField getPackageTextField(){
        return packageText;
    }
    public ArrayList<TextField> getTextFieldArray(){
        return textFieldArray;
    }
    public TableView getVariableTable(){
        return varTable;
    }
    public TableView getMethodTable(){
        return methodTable;
    }
    public ContextMenu getContextSpaceMenu(){
        return spaceError;
    }
    public ContextMenu getContextEmptyMenu(){
        return emptyError;
    }
    public ContextMenu getContextPackageMenu(){
        return packageEmptyError;
    }
    public void setDisableAddButtons(boolean disable){
        Plus.setDisable(disable);
        Minus.setDisable(disable);
    }
   
}
