/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;


import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TextField;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import static pm.data.DataManager.HIGHLIGHTED_STYLE;
import static pm.data.DataManager.NORMAL_STYLE;
import pm.gui.Workspace;
import saf.AppTemplate;


/**
 *
 * @author sajidkamal
 */
public class ClassManager extends TypeChecking  {
    AppTemplate app;
    ArrayList<Variable> varList;
    ArrayList<Method> methodList;
    public GridPane classGrid;
    String className;
    String packageName;
    boolean isAbstract;
    Text classText=new Text();
    ArrayList<TypeChecking> parent;
    private static final String CLASS="Class";
    public static final int CLASS_NAME_ROW=0;
    public static final int VARIABLE_ROW=1;
    public static final int METHOD_ROW=2;
    private static final String defaultClass= "DEFAULT_CLASS_NAME";
    
    
    public ClassManager(double x, double y){
        isAbstract=false;
        parent= new ArrayList<TypeChecking>();
        varList=new ArrayList<Variable>();
        methodList= new ArrayList<Method>();
        classGrid= new GridPane();
        classGrid.setBackground(new javafx.scene.layout.Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        classGrid.setLayoutX(x);
        classGrid.setLayoutY(y);
     
        for (int i = 0; i <= 2; i++) {
            RowConstraints con = new RowConstraints();
            con.setPrefHeight(50);
            classGrid.getRowConstraints().add(con);
        }
        ColumnConstraints col = new ColumnConstraints();
        col.setPrefWidth(150);
        classGrid.getColumnConstraints().add(col);
        classGrid.setGridLinesVisible(true);
        packageName="default";
        setClassName(defaultClass);
        
        //Variable testVar = new Variable("test","int",true,"public");
        ///varList.add(testVar);
        //String name, String type, boolean staticBool, boolean abstractBool, String access, String args1,String args2 ){
        //Method testMethod =new Method("testMethod","void",true,false,"public","ClassManager c","Rectange c");
        //methodList.add(testMethod);
        
    }
    public void setClassName(String name){
        className=name;
        setTextMethod(name);
        
    }
    public String getClassName(){
        return className;
    }
    
    public void addVariable(Variable var){
        varList.add(var);
        this.setTextMethod(var.toString());
        
    }
    public void addMethod(Method meth){
        methodList.add(meth);
        if(meth.isIsAbstract()){
            this.isAbstract=true;
        }
    }
    public ArrayList<Variable> getVariableList(){
        return varList;
    }
    public ArrayList<Method> getMethodList(){
        return methodList;
    }
    public void setTextMethod(String text){
        classText.setText(text);
        if(!classGrid.getChildren().contains(classText)){
            classGrid.add(classText, 0, CLASS_NAME_ROW);
        }
    }
    public void setPackageName(String s){
        packageName=s;
    }
   public String getPackageName(){
       return packageName;
   }
    @Override
   public String getType(){
       return CLASS;
   }
   public void addParent(TypeChecking s){
       parent.add(s);
   }
   public ArrayList<TypeChecking> getParentList(){
       return parent;
   }
    @Override
   public String getName(){
       return this.getClassName();
   }
   public boolean checkAbstract(){
       return this.isAbstract;
   }
   
   
}

