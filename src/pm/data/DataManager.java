package pm.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.util.Stack;
import javafx.event.EventType;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import pm.file.FileManager;
import saf.components.AppDataComponent;
import pm.gui.Workspace;
import saf.AppTemplate;
import saf.ui.AppMessageDialogSingleton;

/**
 * This class serves as the data management component for this application.
 *
 * @author Sajid Kamal
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    ArrayList<ClassManager> classList= new ArrayList();
    ArrayList<InterfaceManager> interfaceList= new ArrayList();
    public TypeChecking selectedClass=null; 
    double dragX;
    double dragY;
    int index;
    boolean ok=true;
    static final String HIGHLIGHTED_STYLE= "-fx-border-color: yellow; -fx-border-width:4px;";
    static final String NORMAL_STYLE= "-fx-border-color: black; -fx-border-width:1px;";
    String prevString=null;
    ObservableList<Variable> variables;
    ObservableList<Method> methods;
    ObservableList<String> parentList;
    Method selectedMethod;
    
   private double anchorX;
   private double anchorY;
   private int snap22;
   private int snap22Y;
   ArrayList<ParentLine> parentLineList=new ArrayList<>();
   ParentLine selectedLine;
   public boolean changeHandler=false;
  
    
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
    }
    public DataManager(){
        // EMPTY
    }
    public void handleAddClass(Pane leftPane,VBox rightPane,double x,double y) throws IllegalArgumentException{
       Workspace workspace= (Workspace) app.getWorkspaceComponent();
        if(selectedClass!=null){
            selectedClass.getGridPane().setStyle(NORMAL_STYLE);
            selectedClass.hideAnchors(true);
            selectedClass=null;
        }
            ClassManager newClass= new ClassManager(x,y);
            leftPane.getChildren().add(newClass.classGrid);
            leftPane.getChildren().addAll(newClass.anchorSE,newClass.anchorRight,newClass.anchorBottom);
            makeSelected(newClass);
            classList.add(newClass);
            workspace.disableZoomButtons(false);
            
            try{
                checkExists();
            }
            catch(IllegalArgumentException d){
              AppMessageDialogSingleton.getSingleton().show("Error","A class with this name already exists! Please rename this class.");
           }
            
            
            // DISABLE ALL OTHER CLASSES SO YOU CAN ONLY MOVE AROUND THE NEWLY CREATED RECTANGLE
            for(ClassManager c:classList){
                c.classGrid.setDisable(true);
            }
            newClass.classGrid.setDisable(false);
        
            newClass.setAnchorCursors(leftPane);
            loadTable(newClass);
            loadMethodTable(newClass);
            loadParents(newClass);
            
            newClass.classGrid.setOnMousePressed(e->{
                dragX=e.getSceneX()-newClass.classGrid.getLayoutX();
                dragY=e.getSceneY()-newClass.classGrid.getLayoutY();
            });
            newClass.classGrid.setOnMouseDragged(e1->{

                newClass.classGrid.setLayoutX(e1.getSceneX()-dragX);
                newClass.classGrid.setLayoutY(e1.getSceneY()-dragY);
                for(Variable v: newClass.getVariableList()){
                if(v.packageBox!=null){
                    v.packageBox.arrowHead.setX(newClass.classGrid.getLayoutX()+v.packageBox.getOffset());
                }
            }
            
            
            if(workspace.snapBool){
            
            int testX= (int) newClass.classGrid.getLayoutX();
                int testY= (int) newClass.classGrid.getLayoutY();
                for(int i=0;i<22;i++){
                if(testX%22==0){
                  snap22=testX;
                }
                testX++; 
                }
                
                for(int i=0;i<22;i++){
                  if(testY%22==0){
                  snap22Y=testY;
                }
                  testY++;
                }
                newClass.classGrid.setLayoutX(snap22);
                newClass.classGrid.setLayoutY(snap22Y);
            
            }
           
            
            });
            
    }
    
    public void handleAddInterface(Pane leftPane,VBox rightPane,double x, double y) throws IllegalArgumentException{
         Workspace workspace= (Workspace) app.getWorkspaceComponent();
        if(selectedClass!=null){
            selectedClass.getGridPane().setStyle(NORMAL_STYLE);
            selectedClass.hideAnchors(true);
            selectedClass=null;
        } 
            InterfaceManager newInterface= new InterfaceManager();
            newInterface.getGridPane().setLayoutX(x);
            newInterface.getGridPane().setLayoutY(y);
            leftPane.getChildren().add(newInterface.getGridPane());
            leftPane.getChildren().addAll(newInterface.anchorBottom,newInterface.anchorRight,newInterface.anchorSE);
            makeSelected(newInterface);
            interfaceList.add(newInterface);
            workspace.disableZoomButtons(false);
            
            try{
                checkExists();
            }
            catch(IllegalArgumentException d){
              AppMessageDialogSingleton.getSingleton().show("Error","A class with this name already exists! Please rename this class.");
           }
            // DISABLE ALL OTHER CLASSES SO YOU CAN ONLY MOVE AROUND THE NEWLY CREATED RECTANGLE
            for(ClassManager c:classList){
                c.classGrid.setDisable(true);
            }
            for(InterfaceManager i:interfaceList){
                i.interfaceGrid.setDisable(true);
            }
            
            newInterface.getGridPane().setDisable(false);
            newInterface.setAnchorCursors(leftPane);
        
            loadTable(newInterface);
            workspace.getVariableTable().getItems().clear();
            loadMethodTable(newInterface);
            
            newInterface.getGridPane().setOnMousePressed(e->{
                dragX=e.getSceneX()-newInterface.getGridPane().getLayoutX();
                dragY=e.getSceneY()-newInterface.getGridPane().getLayoutY();
            });
            newInterface.getGridPane().setOnMouseDragged(e1->{
                
                newInterface.getGridPane().setLayoutX(e1.getSceneX()-dragX);
                newInterface.getGridPane().setLayoutY(e1.getSceneY()-dragY);
                
                if(workspace.snapBool){
            
            int testX= (int) newInterface.getGridPane().getLayoutX();
                int testY= (int) newInterface.getGridPane().getLayoutY();
                for(int i=0;i<22;i++){
                if(testX%22==0){
                  snap22=testX;
                }
                testX++; 
                }
                
                for(int i=0;i<22;i++){
                  if(testY%22==0){
                  snap22Y=testY;
                }
                  testY++;
                }
                newInterface.getGridPane().setLayoutX(snap22);
                newInterface.getGridPane().setLayoutY(snap22Y);
            
            }
            
            });
        
    }
            

    
    public void selectionToolHandler(){
        Workspace workspace= (Workspace) app.getWorkspaceComponent();
        Pane leftPane = workspace.leftPane;
        
        leftPane.setOnMouseClicked(e->{
            // DO NOTHING
        });
        leftPane.setOnMouseDragged(e->{
        // DO NOTHING
        });
        leftPane.setOnMousePressed(e->{
        // DO NOTHING
        });
        
        // ENABLE ALL THE SHAPES FOR SELECTION
        for(ClassManager d:classList){
            d.getGridPane().setDisable(false);
        }
        for(InterfaceManager i:interfaceList){
            i.getGridPane().setDisable(false);
        }
        
        
        
        for(ClassManager c:classList){
            c.classGrid.setOnMouseClicked(e->{
               
                makeSelected(c);
                loadTable(c);
                loadMethodTable(c);
                loadParents(c);
                
                if(e.getClickCount()==2){
                    c.hideAnchors(false);
                }
                c.setAnchorCursors(leftPane);
            });  
            
           c.classGrid.setOnMousePressed(e->{
               makeSelected(c);
                dragX=e.getSceneX()-c.classGrid.getLayoutX();
                dragY=e.getSceneY()-c.classGrid.getLayoutY();
                
            });
            c.classGrid.setOnMouseDragged(e1->{
            c.classGrid.setLayoutX(e1.getSceneX()-dragX);
            c.classGrid.setLayoutY(e1.getSceneY()-dragY);
            
            for(Variable v: c.getVariableList()){
                if(v.packageBox!=null){
                    v.packageBox.arrowHead.setX(c.classGrid.getLayoutX()+v.packageBox.getOffset());
                }
            }
            
            
            if(workspace.snapBool){
            
            int testX= (int) c.classGrid.getLayoutX();
                int testY= (int) c.classGrid.getLayoutY();
                for(int i=0;i<22;i++){
                if(testX%22==0){
                  snap22=testX;
                }
                testX++; 
                }
                
                for(int i=0;i<22;i++){
                  if(testY%22==0){
                  snap22Y=testY;
                }
                  testY++;
                }
                c.classGrid.setLayoutX(snap22);
                c.classGrid.setLayoutY(snap22Y);
            
            }
           
            });
           c.classGrid.setOnMouseReleased(e->{
            
           });
           
            
        }
        
        for(InterfaceManager i:interfaceList){
            
            i.getGridPane().setOnMouseClicked(e->{
                makeSelected(i);
                workspace.getVariableTable().getItems().clear();
                loadMethodTable(i);
                loadTable(i);
                if(e.getClickCount()==2){
                    i.hideAnchors(false);
                }
                i.setAnchorCursors(leftPane);
                
            });  
            
          i.getGridPane().setOnMousePressed(e->{
               makeSelected(i);
                dragX=e.getSceneX()-i.getGridPane().getLayoutX();
                dragY=e.getSceneY()-i.getGridPane().getLayoutY();
                
            });
            i.getGridPane().setOnMouseDragged(e1->{
            i.getGridPane().setLayoutX(e1.getSceneX()-dragX);
            i.getGridPane().setLayoutY(e1.getSceneY()-dragY);
            
            if(workspace.snapBool){
            
            int testX= (int) i.getGridPane().getLayoutX();
                int testY= (int) i.getGridPane().getLayoutY();
                for(int j=0;j<22;j++){
                if(testX%22==0){
                  snap22=testX;
                }
                testX++; 
                }
                
                for(int j=0;j<22;j++){
                  if(testY%22==0){
                  snap22Y=testY;
                }
                  testY++;
                }
                i.getGridPane().setLayoutX(snap22);
                i.getGridPane().setLayoutY(snap22Y);
            
            }
           
            });
            
            
        }
        
       
        
        
    }
    
    
    public void handleRemoveButton(Pane leftPane){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        if(selectedClass!=null){
        leftPane.getChildren().removeAll(selectedClass.getGridPane(),selectedClass.getAnchorBottom(),selectedClass.getAnchorRight(),selectedClass.getAnchorSE());
        if(selectedClass.getType().equals("Class")){
        classList.remove(selectedClass);
        }
        else if(selectedClass.getType().equals("Interface")){
            interfaceList.remove(selectedClass);
        }
        }
        selectedClass=null;
        
        if(selectedClass==null){
            workspace.disableButtons(true);
        }
        
        if(classList.isEmpty()&interfaceList.isEmpty()){
            workspace.disableButtons(true);
            workspace.disableZoomButtons(true);
        }
        
        
    }
    public ArrayList<ClassManager> getClassList(){
        return classList;
    }
    public ArrayList<InterfaceManager> getInterfaceList(){
        return interfaceList;
    }
   
    public void makeSelected(TypeChecking d) throws IllegalArgumentException{
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        ContextMenu checker= workspace.getContextSpaceMenu();
        ContextMenu empty = workspace.getContextEmptyMenu();
        ContextMenu packageEmpty= workspace.getContextPackageMenu();
       if(selectedClass!=null){
           selectedClass.getGridPane().setStyle(NORMAL_STYLE);
           selectedClass.hideAnchors(true);
           selectedClass=null;
       }
                selectedClass=d;
                selectedClass.getGridPane().setStyle(HIGHLIGHTED_STYLE);
                TextField classNameText= workspace.getClassTextField();
                classNameText.setText(selectedClass.getName());
                TextField packageNameText = workspace.getPackageTextField();
                packageNameText.setText(selectedClass.getPackageName());
                
                
                
        classNameText.setOnAction(e1->{
            if(selectedClass!=null){
                    prevString=selectedClass.getName();
                    try{
                        selectedClass.setName(classNameText.getText());
                        checkExists();
                            if(classNameText.getText().contains(" ")){
                                empty.hide();
                                checker.show(classNameText, Side.BOTTOM, 10, 0);
                            }
                            else if (classNameText.getText().isEmpty()){
                                checker.hide();
                                empty.show(classNameText,Side.BOTTOM,10,0);
                            }
                            else{
                                checker.hide();
                                empty.hide();
                            }
              
                    }
                    catch(IllegalArgumentException s){
                        selectedClass.setName(prevString);
                        classNameText.setText(prevString);
                        AppMessageDialogSingleton.getSingleton().show("Error","A class/interface with this name already exists! Please rename this class.");
                    }
                    
                    
                       
            }
       }); 
                
        packageNameText.setOnAction(e1->{
            if(selectedClass!=null){
                prevString=selectedClass.getPackageName();
                try{
                            
                    selectedClass.setPackageName(packageNameText.getText());  
                    checkExists();
                    if (packageNameText.getText().isEmpty()){      
                            packageEmpty.show(packageNameText,Side.BOTTOM,10,0);
                    }
                    else{
                            packageEmpty.hide();
                    }
                }
                catch(IllegalArgumentException k){
                    selectedClass.setPackageName(prevString);
                    packageNameText.setText(prevString);
                    AppMessageDialogSingleton.getSingleton().show("Error","A class/Interface with this name already exists! Please rename this class.");
                }
                        
             }
        });
        workspace.disableButtons(false);

       
    }
    
    public void handleExportCode() throws IOException{
        FileManager manage = new FileManager();
        FileChooser fc=new FileChooser();
       fc.setInitialFileName("Pose");
       File selectedFile = fc.showSaveDialog(app.getGUI().getWindow());
       selectedFile.mkdir();
       manage.exportData(this, selectedFile.getPath());

		
       
    }
    public void checkExists() throws IllegalArgumentException{
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        TextField classNameText= workspace.getClassTextField();
        TextField packageNameText = workspace.getPackageTextField();
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        ArrayList<TypeChecking> temp = new ArrayList<TypeChecking>();
        temp.addAll(classList);
        temp.addAll(interfaceList);

       for (int i = 0; i < temp.size(); i++) {
           for (int j = i+1; j < temp.size(); j++) {
                if(temp.get(i).getName().equals(temp.get(j).getName())&&temp.get(i).getPackageName().equals(temp.get(j).getPackageName())){
                throw new IllegalArgumentException();
                }
    
            }
        }
    }
   
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        Workspace workspace= (Workspace) app.getWorkspaceComponent();
        Pane leftPane = workspace.leftPane;
            selectedClass=null;
            classList.clear();
            interfaceList.clear();
            leftPane.getChildren().clear();
            
        for(TextField t: workspace.getTextFieldArray()){
                t.clear();
        }
            
    }
    public void loadClasses(ArrayList<ClassManager> classes){
        if(app!=null){
        Workspace workspace= (Workspace)app.getWorkspaceComponent();
        Pane left= workspace.leftPane;
        left.getChildren().clear();
        for(ClassManager c: classes){
            left.getChildren().add(c.classGrid);
            }
        }
    }
    
    public void loadTable(TypeChecking c){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        TableView<Variable> varTable = workspace.getVariableTable();
        if(c.getType().equals("Class")){
         varTable.setDisable(false);
         workspace.setDisableAddButtons(false);
         ClassManager d= (ClassManager) c;
        if(variables!=null){
            variables.clear();
        }
         variables = FXCollections.observableArrayList(d.getVariableList());
        varTable.setItems(variables);
        }
        else{
            varTable.setDisable(true);
            workspace.setDisableAddButtons(true);
        }
      
       
        
    }
    
    public void loadMethodTable(TypeChecking c){
         Workspace workspace = (Workspace) app.getWorkspaceComponent();
        TableView<Method> methodTable = workspace.getMethodTable();
        if(methods!=null){
            methods.clear();
        }
        methods= FXCollections.observableArrayList(c.getMethodList());
        methodTable.setItems(methods);
        
        if(c.getType().equals("Class")){
        ClassManager d = (ClassManager) c;
        boolean containsAbs= false;
        for(Method m:d.getMethodList()){
            if(m.isIsAbstract()){
                containsAbs=true;
            }
        }
        if(containsAbs&!d.nameBox.getChildren().contains(d.abstractText)){
            d.nameBox.getChildren().add(d.abstractText);
        }
        else if (!containsAbs){
            if(d.nameBox.getChildren().contains(d.abstractText)){
            d.nameBox.getChildren().remove(d.abstractText);
        }
        }
    }
    }
    public void addMethod(){
        if(selectedClass!=null){
            selectedClass.addMethod(new Method("test","void",false,false,"public"));
            loadMethodTable(selectedClass);
        }
        
    }
    public void handleRemoveMethod(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        if(workspace.getMethodTable().getItems().size()==1){
            selectedMethod = (Method) workspace.getMethodTable().getSelectionModel().getSelectedItem();
        }
        else{
            workspace.getMethodTable().getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldvalue, Object newValue) { 
                selectedMethod = (Method) newValue;
                
            }
        });
        }
            if(selectedMethod!=null){
                
            selectedClass.getMethodList().remove(selectedMethod);
            selectedClass.getMethodBox().getChildren().remove(selectedMethod.methodText);
            loadMethodTable(selectedClass);
            }
        
    }
    
    public ArrayList<String> importsWithin(ClassManager c){
        ArrayList<String> imports = new ArrayList<String>();
        for(TypeChecking t:c.getParentList()){
            String z = t.getPackageName()+"."+t.getName();
            imports.add(z);
        }
        for(Variable v:c.getVariableList()){
            for(ClassManager d: classList){
                if(v.getType().equals(d.getName())){
                    String y = d.getPackageName()+"."+d.getClassName();
                    imports.add(y);
                }
            }
        }
        
        
        /*
        for(ClassManager c: classList){
            String z= c.getPackageName()+"."+c.getClassName();
            imports.add(z);
        }
        for(InterfaceManager i: interfaceList){
            String y = i.getPackageName()+"."+i.getInterfaceName();
            imports.add(y);
        }*/
        return imports;
    }
        public void handleRemoveVar(){
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            Variable selectedVariable= (Variable) workspace.getVariableTable().getSelectionModel().getSelectedItem();
            
          if(selectedVariable!=null){
                
          if (selectedVariable.packageBox!=null){
            workspace.leftPane.getChildren().remove(selectedVariable.packageBox.stack);
            workspace.leftPane.getChildren().remove(selectedVariable.packageBox.lineGroup);
            }      
                
            ClassManager select = (ClassManager) selectedClass;
            select.getVariableList().remove(selectedVariable);
            select.varBox.getChildren().remove(selectedVariable.varTextBox);
            loadTable(select);
            
            
            
            }
        }
        
        public void handleAddVar(){
      if(variables!=null& selectedClass!=null){
          ClassManager select = (ClassManager) selectedClass;
          select.addVariable(new Variable("default","int",false,""));
          loadTable(select);
          
      }
    }
        
        public void disableTableControls(boolean disable){
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
                workspace.getVariableTable().setDisable(disable);
 
        }
        public void addPackageBox(Variable v){
           Workspace workspace=  (Workspace) app.getWorkspaceComponent();
           boolean contains=false;
           
           ClassManager c = (ClassManager) selectedClass;
            for(Variable var: c.varList){
                if(var.packageBox!=null){
                    if(var.packageBox.getName().equals(v.getType())){
                        contains=true;
                    }  
                }
            }
           if(!contains){
        v.setPackageBox((ClassManager)selectedClass,workspace.leftPane);
           }

        }
        public void loadParents(ClassManager c){
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            ArrayList<String> classString = new ArrayList<>();
            for(ClassManager classes: classList){
                if(!classes.equals(c)){
                    classString.add(classes.getClassName());
                }
            }
            for(InterfaceManager i: interfaceList){
                classString.add(i.getInterfaceName());
            }
            parentList=FXCollections.observableArrayList(classString);
            workspace.getParentComboBox().getItems().clear();
            workspace.getParentComboBox().getItems().addAll(parentList);

            if(!selectedClass.getParentList().isEmpty()){
           
            changeHandler= true;    
            for(TypeChecking k: selectedClass.getParentList()){
            workspace.getParentComboBox().getCheckModel().check(k.getName());
            }
            changeHandler=false;
           
            }

        }
        
        public void findParent(String parentString){
            TypeChecking parentObj = null;
            
            for(ClassManager c: classList){
                if(c.getClassName().equals(parentString)){
                    parentObj= c;
                }
            }
            for(InterfaceManager i: interfaceList){
                if(i.getInterfaceName().equals(parentString)){
                    parentObj=i;
                }
            }
            selectedClass.addParent(parentObj);
            addParentLines(parentObj);
        }
        
        public void addParentLines(TypeChecking obj){
            
            Workspace workspace= (Workspace) app.getWorkspaceComponent();
          
          ParentLine parentConnector = new ParentLine(selectedClass, obj);
          parentLineList.add(parentConnector);
           workspace.leftPane.getChildren().add(parentConnector);
           
        }
        public void removeParent(String parent){
            
            Workspace workspace= (Workspace) app.getWorkspaceComponent();
            TypeChecking d=null;
            for(TypeChecking c: selectedClass.getParentList()){
                if(c.getName().equals(parent)){
                    d=c;
                }
            }
            for (Iterator<ParentLine> iterator = parentLineList.iterator(); iterator.hasNext(); ) {
                ParentLine value = iterator.next();
                    if (value.getChild().getName().equals(selectedClass.getName())&value.getParentClass().getName().equals(d.getName())) {
                        workspace.leftPane.getChildren().remove(value);
                        if(value.anchorParent!=null){
                        workspace.leftPane.getChildren().remove(value.anchorParent);
                        }
                        
                        iterator.remove();
                            
                }
            }
            selectedClass.getParentList().remove(d);
           
            
            
        }
        
        public void undoHandler(){
            

            Workspace workspace= (Workspace) app.getWorkspaceComponent();
            for(ParentLine p: parentLineList){
                
                p.setOnMouseClicked(e->{
                if(e.getClickCount()>1){
                    selectedLine=null;
                    selectedLine=p;
                    System.out.println(selectedLine);
                    selectedLine.splitLine(workspace.leftPane,parentLineList);
                }
                
                });
        }
        }
        
    }

