/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;


import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author sajidkamal
 */
public class ParentLine extends Line {
   private TypeChecking child;
   private TypeChecking parent;
   Circle anchorParent;
   public ParentLine(TypeChecking child, TypeChecking parent){

       setChild(child);
       setParent(parent);
       this.startXProperty().bind(child.getGridPane().layoutXProperty());
       this.startYProperty().bind(child.getGridPane().layoutYProperty());
       
       this.endXProperty().bind(parent.getGridPane().layoutXProperty());
       this.endYProperty().bind(parent.getGridPane().layoutYProperty());
       
       this.setStrokeWidth(5);
   }
   public ParentLine(){
       this.setStrokeWidth(5);
       this.setStroke(Color.BLUE);
       
   }
   public void setChild(TypeChecking childClass){
       child=childClass;
   }
   public void setParent(TypeChecking parentClass){
       parent= parentClass;
   }
   public TypeChecking getChild(){
       return child;
   }
   public TypeChecking getParentClass(){
       return parent;
   }
   public void splitLine(Pane leftPane,ArrayList<ParentLine> lineArray){
       double x= (this.getStartX()+this.getEndX())/2;
       double y= (this.getStartY()+this.getEndY())/2;


       
       Circle anchor = new Circle();
       anchor.setCenterX(x);
       anchor.setCenterY(y);
       anchor.setRadius(6);
       anchor.setFill(Color.AQUA);
       

       
      this.endXProperty().unbind();
      this.endYProperty().unbind();
       
       
       this.setEndX(anchor.getCenterX());
       this.setEndY(anchor.getCenterY());
       this.endXProperty().bind(anchor.centerXProperty());
       this.endYProperty().bind(anchor.centerYProperty());
      
       ParentLine extender = new ParentLine();
       extender.setChild(child);
       extender.setParent(parent);
       
       
      extender.startXProperty().bind(anchor.centerXProperty());
      extender.startYProperty().bind(anchor.centerYProperty());
       
     
      if(anchorParent==null){
      extender.endXProperty().bind(parent.getGridPane().layoutXProperty());
      extender.endYProperty().bind(parent.getGridPane().layoutYProperty());
      }
      else{
      extender.endXProperty().bind(anchorParent.centerXProperty());
      extender.endYProperty().bind(anchorParent.centerYProperty());
      }
      
      if(anchorParent!=null){
      anchorParent.toFront();
      }
      
       leftPane.getChildren().addAll(extender,anchor);
       
       extender.anchorParent=anchorParent;
       
       this.anchorParent=anchor;
       
       
       lineArray.add(extender);
       System.out.println(lineArray.size());
       
       
       setAnchorEvents(anchor);
   }
   public void setAnchorEvents(Circle anchor){
       anchor.setOnMouseDragged(e->{
           anchor.setCenterX(e.getX());
           anchor.setCenterY(e.getY());
       });
   }
   
   
   
}
