/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author sajidkamal
 */
public class Method {
    
    private String methodName;
    private String returnType;
    private boolean isStatic;
    private boolean isAbstract;
    private String access;
    private Variable arg1;
    private Variable arg2;
    ArrayList<Variable> methodVarList;
    private static final ArrayList<String> primitiveList= new ArrayList<String>(Arrays.asList("int","double","float","byte","short","boolean","char"));
    
    public Method(String name, String type, boolean staticBool, boolean abstractBool, String access){
        this.methodName=name;
        this.returnType=type;
        this.isStatic=staticBool;
        this.isAbstract=abstractBool;
        this.access=access;
        methodVarList= new ArrayList<Variable>();
}
    

public void setMethodName(String name){
    this.methodName=name;
}
public void setReturnType(String type){
    this.returnType=type;
}
public void setStatic(boolean staticBool){
    this.isStatic=staticBool;
}
public void setAbstract(boolean abstractBool){
    this.isAbstract=abstractBool;
}
public void setAccess(String access){
    this.access=access;
}
public void addArg(Variable var){
    methodVarList.add(var);
}
public String getMethodName(){
        return methodName;
    }

/**
     * @return the returnType
     */
    public String getReturnType() {
        return returnType;
    }

    /**
     * @return the isStatic
     */
    public boolean isIsStatic() {
        return isStatic;
    }

    /**
     * @return the isAbstract
     */
    public boolean isIsAbstract() {
        return isAbstract;
    }

    /**
     * @return the access
     */
    public String getAccess() {
        return access;
    }
    public ArrayList<Variable> getMethodArgs(){
        return methodVarList;
    }

    /**
     * @return the arg1
     */
   
    public String methodCodeString(){
        String code= this.getAccess()+" ";
        if(this.isAbstract){
            code+= " abstract ";
                }
        if(this.isStatic){
            code+=" static ";
        }
        if(!this.getReturnType().equals("null")){
        code+= this.getReturnType()+" ";
        }
        code+=this.getMethodName()+"( ";
        
        for(int i=0; i<methodVarList.size();i++){
            code+= methodVarList.get(i).getType() +" "+ methodVarList.get(i).getName();
            if(i!=methodVarList.size()-1){
                code+=" , ";
            }
        }
        /*for(Variable var: methodVarList){
           code+= var.getType()+" "+var.getName();
           if(methodVarList.size()>1 & methodVarList.iterator().hasNext()){
               code+=" , ";
           }
        }*/
        code+= ") { \n";
        if(!this.getReturnType().equals("void")& !primitiveList.contains(this.getReturnType())){
            code+= "\n return null;";
        }
        else if(primitiveList.contains(this.returnType)){
            code+= " return 1";
        }
        code+="\n}";

       return code;
    }
    
     public String methodIntCodeString(){
        String code= this.getAccess()+" ";
        if(this.isAbstract){
            code+= " abstract ";
                }
        if(this.isStatic){
            code+=" static ";
        }
        if(!this.getReturnType().equals("null")){
        code+= this.getReturnType()+" ";
        }
        code+=this.getMethodName()+"( ";
         for(Variable var: methodVarList){
           code+= var.getType()+" "+var.getName();
           if(methodVarList.size()>1 & methodVarList.listIterator().next()!=null){
               code+=",";
           }
        }
        code+= "); ";

       return code;
    }


     
   


}