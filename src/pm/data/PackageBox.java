/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.scene.Group;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import pm.gui.packageImportDialog;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author sajidkamal
 */
public class PackageBox {
    StackPane stack;
    Rectangle packageRect;
    Rectangle arrowHead;
    private Text packageText;
    private String packageName;
    private String importName;
    Line connector;
    Group lineGroup;
    private double dragX;
    private double dragY;
    private double dragXX;
    private double offset;
    
    
    
    public PackageBox(ClassManager selectedClass){
        stack = new StackPane();
        
        
        
        packageRect= new Rectangle();
        packageRect.setHeight(80);
        packageRect.setWidth(100);
        packageRect.setFill(Color.WHITE);
        packageRect.setStroke(Color.BLACK);
        packageRect.setStrokeWidth(3);
        
        
        stack.setLayoutX(selectedClass.anchorBottom.getCenterX());
        stack.setLayoutY(selectedClass.anchorBottom.getCenterY()+200);
        packageName="defaultPackage";
        packageText= new Text();
        packageText.setText(packageName);
        
        
        stack.getChildren().addAll(packageRect,packageText);
        lineGroup = new Group();
        lineGroup = makeConnectorLine(selectedClass);
        
        setActionHandler();
        
        
        
    }
    
    public void setActionHandler(){
        
         this.stack.setOnMouseClicked(e->{
             
             if(e.getClickCount()>1){
        packageImportDialog dialog= new packageImportDialog(false);
            dialog.show(this);
             }
        });
         
         this.stack.setOnMousePressed(e->{
                dragX=e.getSceneX()-this.stack.getLayoutX();
                dragY=e.getSceneY()-this.stack.getLayoutY();
            });
            this.stack.setOnMouseDragged(e1->{
               
            
            this.stack.setLayoutX(e1.getSceneX()-dragX);
            this.stack.setLayoutY(e1.getSceneY()-dragY);
            
            });
    }
    
      public void setName(String name){
          packageName=name;
        packageText.setText(name);
        
    }
      public String getName(){
          return packageName;
      }
    public void setImportName(String name){
        importName= name;
    }
    public String getImportName(){
        return importName;
       }
    public void setPackage(String className){
         Package[] pack = Package.getPackages();
        String y = className;
       String imp = null;

        for (int i = 0; i < pack.length; i++) {

            Package t = Package.getPackage(pack[i].getName());
            try {
                Class x = Class.forName(t.getName() + "." + y, false, ClassLoader.getSystemClassLoader());
                imp = x.getName();

            } catch (ClassNotFoundException s) {
                // GO BACK TO LOOP
            }

        }
        if(imp==null){
            throw new IllegalArgumentException();
        }
        else{
            setImportName(imp);
        }
        
        
    }
    public Group makeConnectorLine(ClassManager c){
        
        connector= new Line();
        connector.setStrokeWidth(3);
        
        
        
        arrowHead= new Rectangle();
        arrowHead.setFill(Color.DARKGREY);
        
        
        
        arrowHead.setX(c.anchorBottom.getCenterX());
        arrowHead.setY(c.anchorBottom.getCenterY());
        arrowHead.setHeight(40);
        arrowHead.setWidth(40);
        
        arrowHead.setRotate(45);
        
        //connector.setStartX(stack.getLayoutX());
        //connector.setStartY(stack.getLayoutY());
        
        connector.startXProperty().bind(stack.layoutXProperty().add(stack.widthProperty().divide(2)));
        connector.startYProperty().bind(stack.layoutYProperty());
        
        
        //connector.setEndX(arrowHead.getX()+arrowHead.getWidth());
        //connector.setEndY(arrowHead.getY()+arrowHead.getHeight());
        
        connector.endXProperty().bind(arrowHead.xProperty().add(arrowHead.widthProperty().subtract(18)));
        connector.endYProperty().bind(arrowHead.yProperty().add(arrowHead.heightProperty()));
        
        
   
        
       //arrowHead.xProperty().bind(c.anchorBottom.centerXProperty());
       
       
       arrowHead.yProperty().bind(c.anchorBottom.centerYProperty());
       
       arrowHead.setOnMousePressed(e->{
       dragXX=e.getX()-arrowHead.getX();
       });
       
       arrowHead.setOnMouseDragged(e->{
           
       if(e.getX()>c.classGrid.getLayoutX()& e.getX()<c.classGrid.getLayoutX()+c.classGrid.getWidth()-5)
       arrowHead.setX(e.getX()-dragXX);
       offset= arrowHead.getX()-c.classGrid.getLayoutX();
       
       
       });
       
        
        
        
        Group test = new Group();
        test.getChildren().addAll(arrowHead,connector);
        return test;
    }
    
    public double getOffset(){
        return offset;
    }
}
