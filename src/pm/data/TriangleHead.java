/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;

/**
 *
 * @author sajidkamal
 */
public class TriangleHead extends Polygon {
    Circle anchor;
    
    public TriangleHead(double x,double y,TypeChecking parent){
        calculatePoints(x,y);
        this.setFill(Color.DARKGRAY);
        anchor= new Circle();
        anchor.setCenterX(x);
        anchor.setCenterY(y-40);
        anchor.setRadius(5);
        anchor.setFill(Color.DARKCYAN);
        
        this.setOnMouseDragged((e->{
            if(e.getX()>parent.getGridPane().getLayoutX()& e.getX()<parent.getGridPane().getLayoutX()+parent.getGridPane().getWidth()){
                calculatePoints1(e.getX(),parent.getGridPane().getLayoutY());
            }
        
        }));
        
 
        parent.getGridPane().layoutXProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                
               
              calculatePoints1(parent.getGridPane().getLayoutX()+(parent.getGridPane().getWidth()/2),parent.getGridPane().getLayoutY());
            }
        });
        
        parent.getGridPane().layoutYProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {   
             calculatePoints1(parent.getGridPane().getLayoutX()+(parent.getGridPane().getWidth()/2),parent.getGridPane().getLayoutY());
            }
        });
        

        
        
        
    }
    public void calculatePoints(double x,double y){
        
        this.getPoints().addAll(new Double[]{
            x-25, y-40,
            x+25, y-40,
            x   ,    y    });
    }
    public void calculatePoints1(double x,double y){
        this.getPoints().remove(0, 6);
        this.getPoints().addAll(new Double[]{
            x-25, y-40,
            x+25, y-40,
            x   ,    y    });
        
        anchor.setCenterX(x);
        anchor.setCenterY(y-40);
    }
    public double calculateOffset(){
        double offset= this.getPoints().get(4);
        
        return offset;
    }
    
    
}
