/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.test_bed;

import java.io.IOException;
import java.util.ArrayList;
import pm.data.ClassManager;
import pm.data.DataManager;
import pm.data.InterfaceManager;
import pm.data.Method;
import pm.data.Variable;
import pm.file.FileManager;
import saf.AppTemplate;
import saf.controller.AppFileController;

/**
 *
 * @author sajidkamal
 */
public class Test_Save {
 
public static void main(String[] args) throws IOException{
    DataManager data = new DataManager();
    ArrayList<ClassManager> classArray = data.getClassList();
    ArrayList<InterfaceManager> interfaceArray= data.getInterfaceList();
    ClassManager testing= new ClassManager(300,500);
        testing.setClassName("ThreadExample");
        testing.setPackageName("testPackage");
        
        //String name, String type, Boolean staticBool, String access
        Variable test1 = new Variable("START_TEXT","String",true,"public");
        testing.addVariable(test1);
        Variable test2 = new Variable("PAUSE_TEXT","String",true,"public");
        testing.addVariable(test2);
        Variable test3= new Variable("window","Stage",false,"private");
        testing.addVariable(test3);
        Variable test4 = new Variable("appPane","BorderPane",false,"private");
        testing.addVariable(test4);
        
        //String name, String type, boolean staticBool, boolean abstractBool, String access, String args1,String args2 
        
        Method meth1 = new Method("startWork","void",false,false,"public","null","null");
        Method meth2= new Method("pauseWork","void",false,false,"public","null","null");
        testing.addMethod(meth1);
        testing.addMethod(meth2);
        
        ClassManager testing2 = new ClassManager(300,400);
        testing2.setClassName("CounterTask");
        testing2.setPackageName("test");
        
        Variable test5 = new Variable("app","ThreadExample",false,"private");
        Variable test6= new Variable("counter","int",false,"private");
        testing2.addVariable(test5);
        testing2.addVariable(test6);
        //String name, String type, boolean staticBool, boolean abstractBool, String access, String args1,String args2 
        Method testMeth = new Method("CounterTask","ThreadExample",false,false,"public","initApp","null");
        Method testMeth1= new Method("call","Void",false,false,"protected","null","null");
        testing2.addMethod(testMeth);
        testing2.addMethod(testMeth1);  
        
        
        
        ClassManager testing3 = new ClassManager(400,500);
        testing3.setClassName("DateTask");
        testing3.setPackageName("test.do");
        
        //String name, String type, Boolean staticBool, String access
        Variable ThreadExample = new Variable("app", "ThreadExample", false,"private");
        Variable now = new Variable("now","Date",false,"private");
        testing3.addVariable(ThreadExample);
        testing3.addVariable(now);
       
        ////String name, String type, boolean staticBool, boolean abstractBool, String access, String args1,String args2 
        Method dateTask = new Method("DateTask","null",false,false,"public","ThreadExample initApp","null");
        Method call = new Method("call","void",false,false,"protected","null","null");
        testing3.addMethod(dateTask);
        testing3.addMethod(call);
        
        ClassManager testing4 = new ClassManager(300,400);
        testing4.setClassName("StartHandler");
        testing4.setPackageName("test.so");
        
        Variable app = new Variable("app","ThreadExample",false,"private");
        testing4.addVariable(app);
        
        Method StartHandler= new Method("StartHandler","null",false,false,"public","ThreadExample initApp","null");
        Method handle= new Method("handle","void",false,false,"public","Event event","null");
        testing4.addMethod(StartHandler);
        testing4.addMethod(handle);
        
        InterfaceManager EventHandler = new InterfaceManager();
        EventHandler.setInterfaceName("EventHandler");
        EventHandler.setPackageName("test.po");
        EventHandler.addMethod(handle);
        EventHandler.setLocationX(400);
        EventHandler.setLocationY(700);
        
        
        interfaceArray.add(EventHandler);
        
        
        
        
        
        
        classArray.add(testing);
        classArray.add(testing2);
        classArray.add(testing3);
        classArray.add(testing4);
        
        FileManager fileManage = new FileManager();
        fileManage.exportData(data, null);
        fileManage.saveData(data, "./work/DesignTest.json");
        
        
       
}
}
    
  
    
 
